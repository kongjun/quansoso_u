<?php
include '../../services/bannerService.php';
include '../../';

$current = isset($_GET['current']) ? intval($_GET['current']) : 1;
$pageSize = 10;

$page = getAllBannerPage($current,$pageSize);
$smarty = new Smarty ();

$smarty->assign("page",$page);
$smarty->assign("menu","other");

$smarty->display ("templates/banner_manage.tpl");

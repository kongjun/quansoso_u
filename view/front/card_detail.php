<?php
include '../../services/cardService.php';
include '../../services/merchantService.php';
include '../../util/arrayUtil.php';
include '../../util/util.php';

if(isset($_GET['id']) || isset($_GET['sorceId'])){
	
	$cardDo = new CardDO();
	if(isset($_GET['id'])){
		$couponId = intval($_GET['id']);
		$cardDo = getCardById($couponId);
	}else {
		$sourceId = intval($_GET['sorceId']);
		$cardDo = getCardBySourceId($sourceId);
	}
	
	$merchant = getMerchantByName($cardDo->merchant);
	/* $recommends = getMerchantRecommendItems($merchant->external_id,18);
	$items = array();
	for ($i=0,$len=count($recommends);$i<$len;$i++){
		
		for ($j=0,$len2=count($recommends[$i]);$j<$len2;$j++){
			
			$item = $recommends[$i][$j];
			$item->item_price = sprintf("%0.2f",$item->item_price);
			$items[] = $item;
		}
		
	} */
	if(empty($merchant->external_id)){
		$merchant->external_id = getMerchantIdByKeyword($cardDo->merchant);
		$respMerchant = getMerchantShopInfo($cardDo->merchant);
		$merchant->external_cid = $respMerchant->cid;
		$merchant->external_shop_id = $respMerchant->sid;
		$merchant->description = $respMerchant->desc;
		if(empty($merchant->name)){
			
			$merchant->name = $respMerchant->nick;
			$merchant->status = 1;
			$merchant->gmt_created = date("Y-m-d",time());
			addMerchant($merchant);
		}else{
			updateMerchantById($merchant);
		}
		
	}
	$items = getMerchantRecommendItems($merchant->external_id,18);
	$line = getWaterFall($items,3);
	
}

$nick = is_null($context->getLoginUser()->getMixNick()) ? 0 : $context->getLoginUser()->getMixNick();
//$nick = "��01wAwxxIxcL28uzuD2oLlS7c2DEMds1FAQI7fgfrP3PMg=";
//$ip = getIPAddress();
$smarty = new Smarty ();

$smarty->assign("card",$cardDo);
$smarty->assign("merchant",$merchant);
$smarty->assign ("line", $line);
$smarty->assign ("nick", $nick);


$smarty->display ( "templates/card_detail.tpl" );


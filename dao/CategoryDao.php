<?php
include '../model/CategoryDO.php';

class CategoryDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description 查询类目通过名称
	 * @param unknown $name
	 * @return CategoryDO
	 */
	public function selectCategoryByName($name)
	{
		$sql = "select * from ekp_category where `name` = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($name));
		return new CategoryDO($exec->fetch());
	}
	
	/**
	 *
	 * @Description 查询类目通过id
	 * @param unknown $name
	 * @return CategoryDO
	 */
	public function selectById($id){
	
		$sql = "select * from ekp_category where id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($id));
		return new CategoryDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 通过菜单字段查询类目
	 * @param unknown $menuLength
	 * @return multitype:CategoryDO
	 */
	public function selectCategoryByIsMenu($menuLength){
		
		$categorys = array();
		$sql = "select * from ekp_category where is_menu is true order by is_menu asc limit 0,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($menuLength));
		while (@$row=$exec->fetch()){
			$categorys[] = new CategoryDO($row);
		}
		return $categorys;
	}
	
	/**
	 * 
	 * @Description 通过levelCode查询旗下子类目
	 * @param unknown $code
	 * @return multitype:CategoryDO
	 */
	public function selectSunByCode($code){
		
		$categorys = array();
		$sql = "select * from ekp_category where level_code like ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($code."%"));
		while (@$row=$exec->fetch()){
			$categorys[] = new CategoryDO($row);
		}
		return $categorys;
	}
	
	/**
	 * 
	 * @Description 查询标签--like
	 * @param unknown $levelCode
	 * @param unknown $levelNum
	 * @param unknown $isShowTag
	 * @return multitype:CategoryDO
	 */
	public function selectCategoryLikeLevelCode($levelCode,$levelNum,$isShowTag){
		
		$categorys = array();
		$sql = "select * from ekp_category where level_code like ? and level_num > ? and is_show_tag = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($levelCode."%",$levelNum,$isShowTag));
		while (@$row=$exec->fetch()){
			$categorys[] = new CategoryDO($row);
		}
		return $categorys;
	}
	
	/**
	 * 
	 * @Description 查询该类目下最大的子类目
	 * @param unknown $parentId
	 * @return CategoryDO
	 */
	public function selectMaxSunByParentId($parentId){
		
		$sql = "select * from ekp_category where parent_id = ? order by level_code desc";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($parentId));
		return new CategoryDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 添加类目
	 * @param unknown $category
	 * @return boolean
	 */
	public function insertCategory($category){
		
		$sql = "insert into ekp_category (name,level_code,parent_id,level_num,gmt_created) values(?,?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($category->name,$category->level_code,$category->parent_id,$category->level_num,$category->gmt_created));
	}
	
	/**
	 * 
	 * @Description 通过levelNum查询类目
	 * @param unknown $levelNum
	 * @return multitype:CategoryDO
	 */
	public function selectByLevelNum($levelNum){
		
		$sql = "select * from ekp_category where level_num = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($levelNum));
		$categories = array();
		while (@$row=$exec->fetch()){
			$categories[] = new CategoryDO($row);
		}
		return $categories;
	}
	
	/**
	 * 
	 * @Description 查询子类目
	 * @param unknown $parentId
	 * @return multitype:CategoryDO
	 */
	public function selectSunByParentId($parentId){
		
		$sql = "select * from ekp_category where parent_id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($parentId));
		$categories = array();
		while (@$row=$exec->fetch()){
			$categories[] = new CategoryDO($row);
		}
		return $categories;
	}
	
}

$categoryDao = new CategoryDao();


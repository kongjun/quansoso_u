<?php
include '../../services/remoteService.php';
include '../../services/cardService.php';
include '../../util/util.php';

$smarty = new Smarty ();

$ip = getIPAddress();
$size = 10;
$cardType = 1;
$highLight = true;

$current = 1;
if(isset($_GET['current'])){
	$current = $_GET['current'];
	$smarty->assign("current",$current);
}

$sort = 0;
if(isset($_GET['sort'])){
	$sort = $_GET['sort'];
	$smarty->assign("sort",$sort);
}

$keyWord = "";
if(isset($_GET['keyWord'])){
	$keyWordCode = $_GET['keyWord'];
	$keyWord = urldecode_utf8($keyWordCode);
	$smarty->assign("keyWord",$keyWordCode);
}

$resp = searchQuansoso($ip,"$cardType","$highLight",$keyWord,"$current","$size","$sort");
$searches = json_decode($resp);
foreach ($searches->cards as $card){
	$card->merchant = preg_replace('/<.*?>/','',$card->merchantName);
	$card->cardStart = date("Y-m-d",strtotime($card->cardStart));
}

$hotCard = getHotCardBySize(3);

$smarty->assign("searches",$searches);
$smarty->assign("hotCard",$hotCard);
$smarty->display ( "templates/searchlist.tpl");



<div class="quansoso_intaobao_warp">
	<!-- 头部 -->
	{include file="../common/header.tpl"}
	
	<div style="clear:both"></div>

	<div class="safe_area" style="margin-top:10px;overflow:hidden;height:auto">
	<!-- search-list start -->
	
	<div class="search-list fl">
		
			<div class="search-list-tab" >
        		<div class="tab-item fl" >券搜搜结果</div>            	
            	<div class="up_pagination fr">
                	<div class="paga-info fl" style="text-decoration:none">
                		<span class="cur">{$searches.current}</span>/<span class="all">{$searches.totalPage}</span>
                	</div>
					{if $searches.current > 1}
                	<div class="p" id="up_prev">
                		<a href="/d/search?current={$searches.current-1}{if $keyWord}&keyWord={$keyWord|escape:"url"}{/if}{if $sort}&sort={$sort}{/if}">上一页<a/>
                	</div>
					{/if}
					{if $searches.current < $searches.totalPage}
                	<div class="n" id="up_next">
                		<a href="/d/search?current={$searches.current+1}{if $keyWord}&keyWord={$keyWord|escape:"url"}{/if}{if $sort}&sort={$sort}{/if}">下一页<a/>
                	</div>
					{/if}
                </div>
                

            	<div class="number fr">共搜到{$searches.total}张券</div>

            	
     		</div>
     		
     		<div class="sift-sort">
                <ul class="sorting-btns fl">
                	<li {if $sort==0 }class="cur"{/if}>
						<div class="sort_submit moren "><a href="/d/search?sort=0{if $keyWord}&keyWord={$keyWord|escape:"url"}{/if}{if $current}&current={$current}{/if}">默认排序</a></div>
                    </li>
                	<li {if $sort==1}class="cur"{/if}>
						<div class="sort_submit moren "><a href="/d/search?sort=1{if $keyWord}&keyWord={$keyWord|escape:"url"}{/if}{if $current}&current={$current}{/if}">折扣</a></div>
	                </li>
                	<li {if $sort==2}class="cur"{/if}>
						<div class="sort_submit moren "><a href="/d/search?sort=2{if $keyWord}&keyWord={$keyWord|escape:"url"}{/if}{if $current}&current={$current}{/if}">等级</a></div>
	                </li>
                </ul>
 				<!--
				<select class="select-quan fr">
					<option selected="selected" value="-1">全部</option>
					<option value="1">优惠券</option>
					<option value="3">包邮卡</option>
					<option value="6">淘券</option>
				</select>
				-->
            </div>
     		
			{if $searches.total<=0}
    		<div class="no-results" style="background:#fff">
        	   <div style="width:360px;margin:0 auto;">
        		<img style="margin-left:120px;margin-top:15px" src="http://img01.taobaocdn.com/imgextra/i1/729839245/T2mEy1XgJXXXXXXXXX_!!729839245.png" heigth="125" width="125">
				<h1 style="font-size:16px;margin-top:15px">很抱歉，没有搜索到关于"{$keyWord}"的优惠信息。</h1>
				<div class=".tips">
					<h2>建议您：</h2>
					<p>1.<span style="margin-left:10px">看看输入的信息是否有误。</span></p>
					<p>2.<span style="margin-left:10px">调整关键词。</span></p>
				
				</div>
				</div>
			</div>
			{/if}
    
	        <ul class="items-list fl">
				{foreach from=$searches.cards item=card}
				<li class="item-li">
					
					<div class="pic fl">
						<a target="_blank" href="" style="position:relative">
								<img src="{$card.cardPicUrl}">
						</a>
					</div>
				
					<div class="info fl" style="padding: 10px 0 0px;height: 149px;">
						<div class="goods-name">

							<a target="_blank" href="/d/coupon?sorceId={$card.cardSourceId}" target="_blank">{$card.cardName}</a>
						</div>
						<div class="shop fl">
							<div class="shop-name fl">
								<a title="{$card.merchant}" href="{$card.merchantWebsiteUrl}" target="_blank" style="color:#040404">{$card.merchantName}</a>
							</div>
							<span class="fl">
								{if $card.merchantType=="B"}
								<img width="15" height="15" src="http://img03.taobaocdn.com/imgextra/i3/857261527/T2WsjXXddbXXXXXXXX_!!857261527.png_30x30.jpg">
								{else}
								<img width="15" height="15" src="http://img02.taobaocdn.com/imgextra/i2/857261527/T274jxXa8aXXXXXXXX_!!857261527.png_30x30.jpg">
								{/if}
							</span>
						</div>
						<div class="validity">有效期至：<span>{$card.cardStart}</span></div>
					</div>
				
					<div class="type fl" style="padding-top: 10px;height: 149px;">
						<a href="" style="color: #F31731;">
							<span style="margin-bottom: 12px;">{$card.cardDenomination/100}元优惠券</span>
	                    </a>
	                   
						<label>满{$card.cardMoneyCondition/100}减{$card.cardDenomination/100}元</label>
					</div>
				</li>
				{/foreach}				      	

			</ul> 
	</div>




   <div class="recommend-cards" style="float:right">
		<div class="title">热券推荐</div>
			
			{foreach from=$hotCard item=card }
			<div class="card">
				<a class="recommand-image" href="/d/coupon?id={$card.id}" title="{$card.name}" style="position:relative"><img src="{$card.pic_url}" ></a>
				<a class="recommand-link" href="/d/coupon?id={$card.id}">{$card.name}</a>
			</div>
			{/foreach}
			
		</div>
		    <!-- seartch-ad end -->
	</div>
	
</div>

<script src="/assets/javascripts/front/search.js?t=20130418"></script>
    
<?php
include '../model/MerchantDO.php';

class MerchantDao {

	protected $pdo;

	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}

	/**
	 *
	 * @Description 通名字获得商家信息
	 * @param unknown $name
	 * @return MerchantDO
	 */
	public function selectMerchantByName($name){

		$sql = "select * from ekp_merchant where name = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($name));
		return new MerchantDO($exec->fetch());
	}

	public function insertMerchant($merchant){

		$sql = "insert into ekp_merchant (name,pic_url,external_id,external_shop_id,type,external_cid,external_category,external_level,brand,website_url,keywords,description,status,memo,gmt_created)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($merchant->name,$merchant->pic_url,$merchant->external_id,$merchant->external_shop_id,$merchant->type,$merchant->external_cid,$merchant->external_category,$merchant->external_level,$merchant->brand,$merchant->website_url,$merchant->keywords,$merchant->description,$merchant->status,$merchant->memo,$merchant->gmt_created));
		return $this->pdo->lastInsertId();
	}
	
	public function selectCountMerchantByName($name){

		$sql = "select count(*) from ekp_merchant where name = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($name));
		return $exec->fetchColumn();
	}
	
	/**
	 * 
	 * @Description 更新商家的externalId
	 * @param unknown $id
	 * @param unknown $externalId
	 * @return boolean
	 */
	public function updateById($merchant){
		
		$sql = "update ekp_merchant set external_id =?,external_shop_id=?,external_cid=?,description=? where id = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($merchant->external_id,$merchant->external_shop_id,$merchant->external_cid,$merchant->description,$merchant->id));
	}

}

$merchantDao = new MerchantDao();
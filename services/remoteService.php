<?php
include '../model/SystemParam.php';
include '../model/HttpClient.php';
include '../model/CardSendRequest.php';
include '../model/CardSearchRequest.php';
include '../model/MerchantSearchRequest.php';
include '../model/CouponSearchRequest.php';
include '../util/arrayUtil.php';
include '../util/paramUtil.php';

function getSignSysParam($service){
	
	$systemParam = new SystemParam ($service);
	$signArr = objectConventArray($systemParam);
	global $secret;
	$signArr['secret'] = $secret;
	$sign = getSignDataByArray($signArr);
	$sign = encryption($sign);
	$systemParam->sign = $sign;
	return $systemParam;
}

function getSignByRequest($request){
	return array_multiToSingle(false,objectConventArray($request));
}

function remote($url,$paramArray){
	return $fetchService->post($url,$paramArray);
}

/**
 * 
 * @Description 同步卡券
 * @param unknown $cardType
 * @param unknown $shopType
 * @param unknown $modified
 * @param unknown $pageSize
 * @param unknown $currentPage
 */
function searchCouponCard($cardType,$shopType,$modified,$pageSize,$currentPage){
	
	$service = "ekupeng_public_card_search";
	$systemParam = new SystemParam ($service);
	$cardSearch = new CardSearchRequest();
	$cardSearch->systemParam = $systemParam;
	$cardSearch->card_type = $cardType;
	$cardSearch->shop_type = $shopType;
	$now = date ( 'Y-m-d', time () );
	$cardSearch->end_date_end = $now;
	$cardSearch->reminder_start = "1";
	$cardSearch->last_modified = $modified;
	$cardSearch->page_size = $pageSize;
	$cardSearch->current_page = $currentPage;
	
	$signArr = getSignByRequest($cardSearch);
	$sign = getSignDataByArray($signArr);
	$sign = encryption($sign);
	$signArr["sign"] = $sign;
	global $repositoryUrl;
	return remote($repositoryUrl,$signArr);
}

/**
 * 
 * @Description 查询商家
 * @param unknown $merchant
 */
function searchMerchant($merchant){
	
	$service = "ekupeng_merchant_search";
	$systemParam = new SystemParam ($service);
	$merchantSearch = new MerchantSearchRequest();
	$merchantSearch->systemParam = $systemParam;
	$merchantSearch->merchant = $merchant;
	
	$signArr = getSignByRequest($merchantSearch);
	$sign = getSignDataByArray($signArr);
	$sign = encryption($sign);
	$signArr["sign"] = $sign;
	$signArr['merchant'] = utf8_encode($signArr['merchant']);
	global $repositoryUrl;
	return remote($repositoryUrl,$signArr);
}

/**
 *
 *
 * @Description 领券
 *
 * @param CardSendRequest $cardSendRequest
 * @return String
 */
function sendCouponCard($cardId,$nick,$uniqId,$outCode) {

	$service = "ekupeng_card_send";
	$systemParam = new SystemParam ($service);
	$sendCard = new CardSendRequest();
	$sendCard->systemParam = $systemParam;
	$sendCard->card_id= $cardId;
	$sendCard->tb_nick = $nick;
	$sendCard->external_uniq_id = $uniqId;
	$sendCard->out_code = $outCode;
	
	$signArr = getSignByRequest($sendCard);
	$sign = getSignDataByArray($signArr);
	$sign = encryption($sign);
	$signArr["sign"] = $sign;
	$signArr['tb_nick'] = utf8_encode($signArr['tb_nick']);
	global $repositoryUrl;
	return remote($repositoryUrl,$signArr);
}

/**
 * 
 * @Description 调用券搜搜的搜索
 * @param unknown $ip
 * @param number $cardType
 * @param string $highLight
 * @param unknown $keywords
 * @param number $currentPage
 * @param number $pageSize
 * @param number $sort
 */
function searchQuansoso($ip,$cardType,$highLight,$keywords,$currentPage="1",$pageSize,$sort="0"){
	
	$request = new CouponSearchRequest();
	$request->setIp($ip);
	$request->setNick("");
	$request->setCardType($cardType);
	$request->setHighLight($highLight);
	$request->setKeywords($keywords);
	$request->setCurrentPage($currentPage);
	$request->setPageSize($pageSize);
	$request->setSort($sort);
	global $quansosoUrl;
	$params = $request->getParams();
	return remote($quansosoUrl,$params);
}


	

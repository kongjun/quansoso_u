
var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

GS.addListener('windowScroll',function(e){
	  var top = e.scrollTop;
	  if(top>=110){
		$(".back-to-top").show();
		$(".move_menu").c_addClass("menutop");
		//$(".quansoso_intaobao_head").c_addClass("menutop");
		
	  }else{
	  	$(".back-to-top").hide();
	  	$(".move_menu").removeClass("menutop");
		//$(".quansoso_intaobao_head").removeClass("menutop");
	  }
	  
	});

/*
var erweimabtn = DOM.query(".erweimabtn")[0];
var erweima = DOM.query(".erweima")[0];

Event.on(erweimabtn, 'mouseenter', function(){
    erweima.style.display = "block";
    erweimabtn.src = "http://img01.taobaocdn.com/imgextra/i1/782694284/T2Q5LwXdtaXXXXXXXX_!!782694284.png";
    
});

Event.on(erweimabtn, 'mouseout', function(){
    erweima.style.display = "none";
    erweimabtn.src = "http://img03.taobaocdn.com/imgextra/i3/782694284/T29IfxXXxaXXXXXXXX_!!782694284.png";
});

/**
 *卡券信息浮动窗
 */
var cardintro = DOM.query(".detail-main-detail-intro-content")[0];
var moreintro = DOM.query(".moreintro")[0];


Event.on(moreintro, 'mouseenter', function(e){
    cardintro.style.left = e.pageX + "px";
    cardintro.style.top = e.pageY - 100 + "px";
    cardintro.style.display = "block";
});

Event.on(moreintro, 'mouseout', function(e){
        //console.log("e.target.className:"+e.target.className);	    
    	if (e.relatedTarget.className == "detail-main-detail-intro-content") {
            return;
        }
        cardintro.style.display = "none";
});

Event.on(cardintro, 'mouseout', function(e){
	//console.log("e.relatedTarget::"+e.relatedTarget.nodeName);
	if (e.relatedTarget.className == "detail-main-detail-intro-content" || e.relatedTarget.nodeName == "A" || e.relatedTarget.nodeName == "B") {
        return;
    }
    cardintro.style.display = "none";
});

/*
 /**
 *弹窗
 */
var TipDivPro = function(){

    function TipCoverDiv(flag){
        var flagdom = DOM.query("." + flag + "")[0];
        
        var tempObj, rootDoc, docH, docW;
        tempObj = DOM.query('.BGCOVERDIV')[0];
        //存在层但是被隐藏
        if (tempObj) {
            //已存在但是隐藏时  打开div层
            
            flagdom.style.display = "block";
            if (tempObj.style.display == 'none') {
                tempObj.style.display = 'block';
            }
            
            //已存在但是隐藏时  打开图片和文字层
            tempObj = DOM.query('.TOP_BGCOVERDIV')[0];
            if (tempObj) {
                if (tempObj.style.display == 'none') {
                    tempObj.style.display = 'block';
                }
            }
            
            //重新创建层
        }
        else {
            rootDoc = document.body || document.documentElement;
            docH = ((rootDoc.clientHeight > rootDoc.scrollHeight) ? rootDoc.clientHeight : rootDoc.scrollHeight);
            docW = ((rootDoc.clientWidth > rootDoc.scrollWidth) ? rootDoc.clientWidth : rootDoc.scrollWidth);
            //蒙版层
            tempObj = document.createElement('div');
            tempObj.setAttribute('class', 'BGCOVERDIV');
            tempObj.style.left = '0px';
            tempObj.style.top = '0px';
            tempObj.style.width = '100%';//docW;
            tempObj.style.height = docH + "px";
            tempObj.style.position = 'absolute';
            tempObj.style.zIndex = 99999;
            tempObj.style.opacity = 0.5;
            tempObj.style.display = 'block';
            tempObj.style.backgroundColor = '#000';
            DOM.query(".detail-main")[0].appendChild(tempObj);
            
            
            var objDivCover = document.createElement('div');
            objDivCover.className = "TOP_BGCOVERDIV";
            flagdom.style.display = "block";
            objDivCover.appendChild(flagdom);
            
            objDivCover.style.position = 'absolute';
            objDivCover.style.zIndex = tempObj.style.zIndex + 300;
            
            document.body.appendChild(objDivCover);
            
            objDivCover.style.left = (docW - objDivCover.offsetWidth) / 2 + 'px';
            
            //console.log("c:"+rootDoc.clientHeight+"----s:"+rootDoc.scrollHeight+"--w:"+window.screen.height);
            objDivCover.style.top = (DOM.query(".detail-main")[0].offsetHeight - objDivCover.offsetHeight) / 2 + 'px';
            
            
            objDivCover.style.background = "#fff";
            
        }
        tempObj = null;
        rootDoc = null;
    }
    /**
     *	隐藏层
     */
    function TipCoverClose(flag){
        var tempObj;
        var flagdom = DOM.query("." + flag + "")[0];
        tempObj = DOM.query('.BGCOVERDIV')[0];
        if (tempObj) {
            tempObj.style.display = 'none';
        }
        
        tempObj = DOM.query('.TOP_BGCOVERDIV')[0];
        if (tempObj) {
            tempObj.style.display = 'none';
        }
        flagdom.style.display = 'none';
    }
    return {
        TipCoverDiv: function(flag){
            TipCoverDiv(flag);
        },
        TipCoverClose: function(flag){
            TipCoverClose(flag)
        }
    }
}();


//TipDivPro.TipCoverDiv("lqdiv");

var exchangebtn = DOM.query('.exchange')[0];
Event.on(exchangebtn, 'click', function(){
	
    var nick = DOM.query(".J_name")[0].value;
    var couponId = DOM.query(".J_couponId")[0].value;
    var date = new Date();
	if(nick==0 || nick=="0"){
		TipDivPro.TipCoverDiv("logintip");
	}else{
		
		nick = encodeURI(nick);
	    KISSY.io({
	        type: "GET",
	        url: "/d/exchange?t=" + date,
	        data: {"nick": nick,"couponId": couponId},
	        dataType: "jsonp",
	        success: function(data, textStatus, xhr){
	        
	            if (data.result == "success") {
	                TipDivPro.TipCoverDiv("lqdiv");
	            }else {
	                DOM.text(".J_callback", data.desc);
	                TipDivPro.TipCoverDiv("lqcallbacktip");
	            }
	        }
	    });
		
	}
});




//clase callback  tip
var closecbtipa = DOM.query('.btn-close')[1];
var closecbtipb = DOM.query('.close-box')[2];
Event.on(closecbtipa, 'click', function(){
    TipDivPro.TipCoverClose("lqcallbacktip");
});

Event.on(closecbtipb, 'click', function(){
    TipDivPro.TipCoverClose("lqcallbacktip");
});


//clase success  tip
var closebtna = DOM.query('.btn-close')[0];
var closebtnb = DOM.query('.close-box')[1];

Event.on(closebtna, 'click', function(){
    TipDivPro.TipCoverClose("lqdiv");
});

Event.on(closebtnb, 'click', function(){
    TipDivPro.TipCoverClose("lqdiv");
});


//clase login tip
var logintipclosebtnb = DOM.query('.close-box')[0];

Event.on(logintipclosebtnb, 'click', function(){
    TipDivPro.TipCoverClose("logintip");
});



/*
var $id = function(o){
    return DOM.query("." + o)[0];
};
var warpWidth = 220; //格子宽度
var margin = 14; //格子间距
function sort(el, childTagName){
    var h = []; //记录每列的高度
    var box = el.getElementsByTagName(childTagName);
    var minH = box[0].offsetHeight;
    var boxW = box[0].offsetWidth + margin;
    
    var n = 950 / boxW | 0; //计算页面能排下多少Pin
    el.style.width = n * boxW - margin + "px";
    DOM.addClass(el, "isVisble");
    for (var i = 0; i < box.length; i++) {//排序算法，有待完善
        var boxh = box[i].offsetHeight; //获取每个Pin的高度
        if (i < n) { //第一行特殊处理
            h[i] = boxh;
            box[i].style.top = 0 + 'px';
            box[i].style.left = (i * boxW) + 'px';
        }
        else {
            minH = Array.min(h); //取得各列累计高度最低的一列
            var minKey = getarraykey(h, minH);
            h[minKey] += boxh + margin; //加上新高度后更新高度值
            box[i].style.top = minH + margin + 'px';
            box[i].style.left = (minKey * boxW) + 'px';
        }
        var maxH = Array.max(h);
        var maxKey = getarraykey(h, maxH);
        el.style.height = h[maxKey] + "px";//定位结束后更新容器高度
    }
    for (var i = 0; i < box.length; i++) {
        DOM.addClass(box[i], "isVisble");
    }
}

Array.min = function(array){
    return Math.min.apply(Math, array);
}
Array.max = function(array){
    return Math.max.apply(Math, array);
}
/* 返回数组中某一值的对应项数 
function getarraykey(s, v){
    var k = null;
    for (k in s) {
        if (s[k] == v) {
            return k;
        }
    }
}
sort($id("wrap"), "div");
*/




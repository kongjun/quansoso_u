
	{include file="../common/message.tpl"}	

<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		
		<form action="/d/activity/add" method="post">
			<table height="450" width="450">
				<tr>
					<td align="right">活动名称:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="title" class="admin_search_input admin_prev_title" /></td>
					<td align="right">商家名称:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="merchant" class="admin_search_input" /></td>
		      	</tr>
				<tr>
	      			<td align="right">开始时间:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="begin" class="admin_search_input" /></td>
					<td align="right">结束时间:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="end" class="admin_search_input" /></td>
		      	</tr>
	      		<tr>
	      			<td align="right">预览图片:</td>
	      			<td width="10"></td>
	      			<td colspan="4"><input type="text" name="img_url" class="admin_input admin_img_prev" /></td>
		      	</tr>
				<tr>
	      			<td align="right">标题:</td>
	      			<td width="10"></td>
	      			<td colspan="4"><input type="text" name="headline" class="admin_input admin_prev_headline" /></td>
		      	</tr>
				<tr>
	      			<td align="right">介绍信息:</td>
	      			<td width="10"></td>
	      			<td colspan="4"><textarea name="explain" cols="70" rows="4" class="admin_explain_prev" ></textarea></td>
		      	</tr>
				<tr>
	      			<td align="right">活动代码:</td>
	      			<td width="10"></td>
	      			<td colspan="4"><textarea name="content" cols="70" rows="8" class="admin_content_html"></textarea></td>
		      	</tr>
	     	 </table>
		
			<div class="admin_save_back" style="width:400px">
					<a><input type="hidden" name="content_html" class="content_html" /></a>
					<a><input type="button" class="admin_save_button admin_preview_img" value="预览图片"></a>
					<a><input type="button" class="admin_save_button admin_preview_content" value="预览活动页面"></a>
					<a><input type="submit" class="admin_save_button" value="创    建"></a>
			</div>
		</form>
	</div>
	<form action="/d/activity/preview" method="post" class="admin_form">
		<input type="hidden" name="content" value="" class="admin_content_prev" />
	</form>	
</div>


<div class="admin_preview_outer"></div>
<div class="admin_preview_inner">
	<p style="float:right;margin-top:-10px;margin-right:-10px;"><a href="#" class="admin_preview_close">关闭</a></p>
	<div class="admin_preview_html">
		<div class="activity-info">
			<img src="" alt="" title="" class="admin_prevImg">
			<div class="status card-ing" style="width:52px;height:52px;left:0;top:0;background:url(http://img02.taobaocdn.com/imgextra/i2/857261527/T2OjNVXkVdXXXXXXXX_!!857261527.png)"></div>
			<div class="active_direc" style="width:295px;color:#69aaa0;font-size:14px;background:#fff;padding-top: 5px;">
				<span style="margin-left:15px" class="admin_prevHeadLine"></span>
			</div>
			<div class="active_direc">
				<div class="directions admin_prevExplain"></div>
			</div>
		</div>
	</div>
</div>




<script src="/assets/javascripts/admin/add_activity.js"></script>
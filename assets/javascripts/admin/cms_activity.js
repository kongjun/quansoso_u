var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});

/**
 * tab效果
 */
var tabs = DOM.query(".admin_tab_title");
var editorTab = 0;
tabs.map(function(item, index){

    Event.on(item, 'click', function(e){
    
        $(".admin_tab_title").removeClass("admin_tab_active");
        $(".admin_tab_title").item(index).c_addClass("admin_tab_active");
        $(".admin_tab_subContent").hide();
        $(".admin_tab_subContent").item(index).show();
        editorTab = index;
    });
    
});


var editorIndex = -1;
var boards = DOM.query(".board");
boards.map(function(item, index){
	
    Event.on(item, 'click', function(){
    	
		var url = DOM.query(".admin_activity_url")[0];
		var img = DOM.query(".admin_activity_img")[0];
		var name = DOM.query(".admin_activity_name")[0];
		url.value = "";
		img.value = "";
		name.value = "";
        $(".admin_outer").show();
        editorIndex = index;
		return false;
    });
    
});


var button = DOM.query(".admin_button")[0];
Event.on(button, 'click', function(){
	
	var url = DOM.query(".admin_activity_url")[0].value;
	var img = DOM.query(".admin_activity_img")[0].value;
	var name = DOM.query(".admin_activity_name")[0].value;
    var date = new Date();
	var board = DOM.query(".board")[editorIndex];
	var item = board.childNodes[0];
	board.href = url;
    item.src = img;
	item.alt = name;
	item.title = name;
	
    $(".admin_outer").hide();
	
});

var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_outer").hide();
});


var save = DOM.query(".admin_save")[0];
Event.on(save, 'click', function(){

    var content = DOM.query(".admin_content")[editorTab];
	var id = DOM.query(".admin_cms_id")[editorTab];
    var contentHtml = DOM.query(".content_html")[0];
    contentHtml.value = content.innerHTML;
	DOM.query(".J_cms_id")[0].value = id.value;
	DOM.query(".J_cms_templateId")[0].value = editorTab;
});


var showMessage = function(){
	
	$(".admin_fadeOut_message").fadeIn(0.5);
	setTimeout(function(){
		$(".admin_fadeOut_message").fadeOut(2);
	},20);
	
}();



<?php

/**
 *DOMAIN的基类
 */

class BaseDO{
     /**
     * convert domain to array
     * @return array
     */
    public function toArray()
    {
        $array = array();
        foreach ($this as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    /**
     * 构造函数
     * @param  $array
     * @return void
     */
     public function __construct($array){
         $this->constructFromArray($array);
     }

    /**
     * construct domain from array
     * @param $array array
     * @return domain
     */
    private  function constructFromArray($array)
    {
        foreach ($this as $key => $value) {
            $value =$array[$key];
            if(is_null($array[$key])){
               $value=  $array[strtoupper($key)];
            }
            $this->$key = $value;
        }
        return $this;
    }

}

/**
 * pdo variable
 * @global
 */
//'jdbc:mysql://192.168.3.188:3306/quansoso_u?characterEncoding=GBK', 'root', 'yikaquan2011'
$PDO = new PDO();
$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


<?php

/**
 * 
 * @Description 合并多为数组
 * @param unknown_type $array
 * @return return_type
 */
function array_multiToSingle($flage,$array) {
	
	static $newArray = array ();
	if(!$flage)
		$newArray = array ();
	
	foreach ( $array as $key => $val ) {
		if (is_array ( $val ) || is_object ( $val )) {
			$oldArray = ( array ) $val;
			$flage = true;
			array_multiToSingle ($flage,$val);
		} else {
			$newArray [$key] = $val;
		}
	}
	return $newArray;
}


/**
 * 
 * @Description 根据键名删除数组项
 * @param unknown $array
 * @param unknown $oldkey
 * @return void|unknown
 */
function deleteByKey($array, $oldkey) {
	
	if (! array_key_exists ( $oldkey, $array ))
		return;
	foreach ( $array as $key => $val ) {
		if ($key == $oldkey) {
			unset ( $array [$key] );
		}
	}
	return $array;
}

/**
 * 
 * @Description 数组转为对象
 * @param Object $obj
 * @return array
 */
function objectConventArray($obj) {
	
	//$reflect = new StdClass ( $obj );
	$pros = get_object_vars($obj);
	
	$array = array ();
	
	foreach ( $pros as $key => $val ) {
		
		if (is_object ( $obj->$key )) {
			$array [$key] = objectConventArray ( $obj->$key );
		} else {
			if(!is_null($obj->$key))
				$array [$key] = $obj->$key;
		}
	}
	
	return $array;
}


function strDateToArray($strDate){
	
	$dateArray = array();
	$time = strtotime($strDate);
	$dateArray['year'] = date("Y",$time);
	$dateArray['month'] = date("m",$time);
	$dateArray['day'] = date("d",$time);
	//$dateArray['week'] = date("N",$time);
	return $dateArray;
}






	

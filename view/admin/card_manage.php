<?php
include '../../services/cardService.php';

$smarty = new Smarty ();

$current = isset($_GET['current']) ? intval($_GET['current']) : 1;

$order = isset($_GET['order']) ? $_GET['order'] : "id";

$now = date("Y-m-d H:m:s",time());
//条件筛选
$filter = "";
if(isset($_REQUEST['status']) && $_REQUEST['status']!=""){
	
	$status = $_REQUEST['status'];
	if($status==0){
		$filter .= " and status = 0";
	}else if($status==1){
		$filter .= " and `end`	>  '".$now."' and status = 1";
	}else{
		$filter .= " and `end`	<  '".$now."' ";
	}
	
	$smarty->assign("status",$status);
}
if(isset($_REQUEST['shopType']) && !empty($_REQUEST['shopType'])){
	
	$shopType = $_REQUEST['shopType'];
	$filter .= " and shop_type = $shopType";
	$smarty->assign("shopType",$shopType);
}
if (isset($_REQUEST['denomination']) && !empty($_REQUEST['denomination'])){
	
	$denominations = $_REQUEST['denomination'];
	$denomination = is_array($denominations) ? join(",", $denominations) : $denominations;
	$filter .= " and denomination in ($denomination)";
	$smarty->assign("denomination",$denomination);
}

//整理数据
$size = 10;
if(isset($_POST['name']) && !empty($_POST['name'])){
	$page = getPageCardLikeMerchantName($_POST['name']);
}elseif(!empty($filter)){
	$page = getCardPageByFilter($filter,$order,$current,$size);
}else{
	$page = getAllCard($size,$current,$order);
}



$lastModified = date("Y-m-d H:m:s",strtotime(getMaxModified()));



$smarty->assign("order",$order);
$smarty->assign("lastModified",$lastModified);
$smarty->assign("now",$now);
$smarty->assign("menu","card");
$smarty->assign("page",$page);

$smarty->display ("templates/card_manage.tpl");


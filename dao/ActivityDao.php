<?php
include '../model/ActivityDO.php';
include '../model/Page.php';

class ActivityDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description 插入活动
	 * @param unknown $activityDo
	 * @return boolean
	 */
	public function insertActivity($activityDo){
		
		$sql = "insert into ekp_activity (title,headline,merchant,img_url,`begin`,`end`,`explain`,content,gmt_created) values(?,?,?,?,?,?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($activityDo->title,$activityDo->headline,$activityDo->merchant,$activityDo->img_url,$activityDo->begin,$activityDo->end,$activityDo->explain,$activityDo->content,$activityDo->gmt_created));
	}
	
	/**
	 * 
	 * @Description 查询所有的活动
	 * @param unknown $current
	 * @param unknown $size
	 * @param unknown $order
	 * @return Page
	 */
	public function selectAllByPage($current,$size,$order){
		
		$sql = "select count(*) from ekp_activity";
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page($size,$current,$totalCount);
		
		$sql = "select id,title,headline,merchant,img_url,`begin`,`end`,`explain` from ekp_activity order by $order desc limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($current-1)*$size,$size));
		$activties = array();
		while (@$row=$exec->fetch()){
			$activties[] = new ActivityDO($row);
		}
		$page->resultList = $activties;
		return $page;
	}
	
	/**
	 * 
	 * @Description 通过id查询活动
	 * @param unknown $id
	 * @return ActivityDO
	 */
	public function selectById($id){
		
		$sql = "select * from ekp_activity where id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($id));
		return new ActivityDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 查询活动---不包括html代码
	 * @param unknown $id
	 * @return ActivityDO
	 */
	public function selectNoContenById($id){
		
		$sql = "select id,title,headline,merchant,img_url,`begin`,`end`,`explain` from ekp_activity where id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($id));
		return new ActivityDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 通过id更新活动
	 * @param unknown $activityDo
	 * @return boolean
	 */
	public function updateById($activityDo){
		
		$sql = "update ekp_activity set title=?,headline=?,merchant=?,img_url=?,`begin`=?,`end`=?,`explain`=?,content=? where id = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($activityDo->title,$activityDo->headline,$activityDo->merchant,$activityDo->img_url,$activityDo->begin,$activityDo->end,$activityDo->explain,$activityDo->content,$activityDo->id));
	}
	
	/**
	 * 
	 * @Description 查询活动内容
	 * @param unknown $merchant
	 * @return ActivityDO
	 */
	public function selectContentByMerchant($merchant){
		
		$sql = "select content from ekp_activity where merchant = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($merchant));
		return new ActivityDO($exec->fetch());
	}
	
}

$activityDao = new ActivityDao();


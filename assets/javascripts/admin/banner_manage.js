

var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});


var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_alertBox").hide();
});

var updateId = -1;
var updateIndex = -1;
var updates = DOM.query(".admin_replace");
var updateImgUrl = DOM.query(".admin_imgUrl")[0];
var updateTitle = DOM.query(".admin_title")[0];
var updateDesc = DOM.query(".admin_describe")[0];
var updateLink = DOM.query(".admin_link")[0];

var title = DOM.query(".admin_itemTitle");
var desc = DOM.query(".admin_itemDesc");
var itemImg = DOM.query(".admin_itemImg");
var link = DOM.query(".admin_itemLink");


updates.map(function(item,index){
	
	Event.on(item,'click',function(){
		
		var itemId = DOM.query(".admin_itemId")[index].innerHTML;
		updateId = itemId;
		updateTitle.value = title[index].innerHTML;
		updateDesc.value = desc[index].innerHTML;
		updateImgUrl.value = itemImg[index].src;
		updateLink.value = (link[index].href).replace(/http:\/\//,"");
		updateIndex = index;
		$(".admin_alertBox").show();
	});
	
});

var button = DOM.query(".admin_button")[0];
Event.on(button, 'click', function(){
	
	var date = new Date();
	var encodeTitle = encodeURI(updateTitle.value);
	var encodeDesc = encodeURI(updateDesc.value);
	var paramLink ="";
	if(updateLink.value!="")
		paramLink = "http://"+updateLink.value;
		
	KISSY.io({
		type:"GET",
	    url:"/d/update_banner_json?t="+date,
	    data: {"id":updateId,"title":encodeTitle,"imgUrl":updateImgUrl.value,"describe":encodeDesc,"link":paramLink},
	    dataType:"jsonp",
		success:function(data, textStatus, xhr){
			
			var message = DOM.query(".admin_fadeOut_message")[0];
			
			if(data.result=="success"){
				title[updateIndex].innerHTML = updateTitle.value;
				desc[updateIndex].innerHTML = updateDesc.value;
				itemImg[updateIndex].src = updateImgUrl.value;
				link[updateIndex].href = "http://"+updateLink.value;
				message.innerHTML = "更新成功！！！";
			}else{
				message.innerHTML = "更新失败！！！";
			}
			$(".admin_alertBox").hide();
			showMessage();
		}
	});
    
});

function showMessage(){
	
	$(".admin_fadeOut_message").fadeIn(0.5);
	setTimeout(function(){
		$(".admin_fadeOut_message").fadeOut(2);
	},20);
	
};





var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});


var cardSyn = DOM.query(".admin_cardSyn")[0];
Event.on(cardSyn,'click',function(){
	
	$(".admin_outer").show();
	var date = new Date();
	KISSY.io({
		type:"GET",
	    url:"/d/card/syn?t="+date,
	    dataType:"jsonp",
		success:function(data, textStatus, xhr){
			
			if(data.result=="success"){
				DOM.text(".message","更行成功，共更新数量："+data.num);
			}else{
				DOM.text(".message","更新失败");
			}
		}
	});
	
});

Event.on(cardSyn,'mouseenter',function(){
	
	$(".admin_updateDate").fadeIn(0.5);
	
});

Event.on(cardSyn,'mouseleave',function(){
	
	$(".admin_updateDate").fadeOut(0.5);
});

var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_outer").hide();
});

var denomination = DOM.query(".admin_denominations")[0].value;
var isChecked = function(){
	
	var denominations = denomination.split(",");
	var denominationCheckboxs = DOM.query(".admin_denomination_checkbox");
	denominations.forEach(function(item){
		
		denominationCheckboxs.map(function(itemBox,index){
			
			if(itemBox.value==item){
				itemBox.checked = "checked";
			}
		});
	});
	
}();


var downs = DOM.query(".admin_cardDown");
downs.map(function(item,index){
	
	Event.on(item,'click',function(){
		
		$(".admin_outer").show();
		var id = DOM.query(".admin_down_id")[index].value;
		var date = new Date();
		KISSY.io({
			type:"GET",
		    url:"/d/card_manage_json?op=down&t="+date,
			data: {"id":id},
		    dataType:"jsonp",
			success:function(data, textStatus, xhr){
				
				if(data.result=="success"){
					DOM.text(".message","更行成功!!");
				}else{
					DOM.text(".message","更新失败!!");
				}
			}
		});
		
	});
	
});


var ups = DOM.query(".admin_cardUp");
ups.map(function(item,index){
	
	Event.on(item,'click',function(){
		
		$(".admin_outer").show();
		var id = DOM.query(".admin_up_id")[index].value;
		var date = new Date();
		KISSY.io({
			type:"GET",
		    url:"/d/card_manage_json?op=up&t="+date,
			data: {"id":id},
		    dataType:"jsonp",
			success:function(data, textStatus, xhr){
				
				if(data.result=="success"){
					DOM.text(".message","更行成功!!");
				}else{
					DOM.text(".message","更新失败!!");
				}
			}
		});
		
	});
	
});
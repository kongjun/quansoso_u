<?php
require_once 'BaseDO.php';

class CmsDO extends BaseDO{
	
	public $id;
	
	public $content;
	
	public $code;
	
	public $sub_template;
	
	public $memo;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}

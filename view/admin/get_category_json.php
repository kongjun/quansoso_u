<?php
include '../../services/categoryService.php';

$callback = $_GET['callback'];
$parenId = $_GET['parentId'];

$categories = getSunCategoryByParenId($parenId);
$jsonCategory = json_encode($categories);
$levelNum = getCategoryById($parenId)->level_num;

$resp = $callback.'({"result":';
$resp.= '"success","categories":'.$jsonCategory.',"levelNum":"'.$levelNum.'"})';

echo $resp;

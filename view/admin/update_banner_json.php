<?php
include '../../services/bannerService.php';

$callback = $_GET['callback'];

$bannerDo = new BannerDO();
$bannerDo->id = $_GET['id'];
$bannerDo->title = urldecode_utf8($_GET['title']);
$bannerDo->describe = urldecode_utf8($_GET['describe']);
$bannerDo->img_url = $_GET['imgUrl'];
$bannerDo->link = $_GET['link'];
$bannerDo->gmt_created = date("Y-m-d",time());

$num = updateBannerById($bannerDo);
$resp = $callback.'({"result":';
if($num){
	$resp.= '"success"})';
}else{
	$resp.= '"error"})';
}

echo $resp;

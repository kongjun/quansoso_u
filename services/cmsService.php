<?php
include '../dao/CmsDao.php';
include '../dao/ItemCodeDao.php';

/**
 * 
 * @Description 添加CMS内容
 * @param unknown $content
 * @param unknown $code
 * @return boolean
 */
function addCms($content,$code) {
	
	global $cmsDao;
	$cmsDo = new CmsDO();
	$cmsDo->content = $content;
	$cmsDo->code = getItemByCode($code);
	$cmsDo->gmt_created = date ( 'Y-m-d H:i', time () );
	return $cmsDao->insert($cmsDo);
}

/**
 * 
 * @Description 通过名称获得CMS内容
 * @param unknown $code
 * @return CmsDO
 */
function getCmsByCode($code){
	
	global $cmsDao;
	$codeNum = getItemByCode($code);
	return $cmsDao->selectOrderByCode($codeNum);
}

function getAllCmsByCode($code){
	
	global $cmsDao;
	$codeNum = getItemByCode($code);
	return $cmsDao->selectAllByCode($codeNum);
}

function updateCmsContentById($id,$content,$subTemplate){
	
	global $cmsDao;
	return $cmsDao->updateContentById($id,$content,$subTemplate);
}



<?php
include '../model/ExchangeDO.php';

class ExchangeDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description ���Ӷһ���Ϣ
	 * @param unknown $exchangeDo
	 * @return boolean
	 */
	public function insert($exchangeDo){
		
		$sql = "insert into ekp_exchange (user_nick,card_id,title,get_type,card_type,channel,consume,consume_type,external_card_id,gmt_created) values(?,?,?,?,?,?,?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($exchangeDo->user_nick,$exchangeDo->card_id,$exchangeDo->title,$exchangeDo->get_type,$exchangeDo->card_type,$exchangeDo->channel,$exchangeDo->consume,$exchangeDo->consume_type,$exchangeDo->external_card_id,$exchangeDo->gmt_created));
	}
	
}

$exchangeDao = new ExchangeDao();


<?php

class Page
{
	public $pageSize = 10;
	
	public $currentPage;
	
	public $totalPage;
	
	public $totalCount;
	
	public $resultList;
	
	/**
	 * 
	 * @Description 页面大小，当前页码，总共纪录
	 * @param $size
	 * @param $current
	 * @param $totalCount
	 */
	public function __construct($size,$current,$totalCount){
		
		$this->pageSize = $size;
		$this->currentPage = $current;
		$this->totalCount = $totalCount;
		$this->totalPage = intval(ceil($totalCount/$size));
	}
	
}
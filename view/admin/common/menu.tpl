<span><input type="hidden" value="{$menu}" class="menu_code"></span>
<div class="admin_menu">
	
	<ul class="container">
	      <li class="menu">
	          <ul>
			    <li class="button"><a href="#" class="green">CMS管理<span></span></a></li>
	            <li class="dropdown">
	                <ul>
	                    <li><a href="/d/cms/recommend">首页淘宝贝推荐</a></li>
	                    <li><a href="/d/cms/coupon">首页优惠券</a></li>
	                    <li><a href="/d/cms/activity">首页推荐活动</a></li>
	                </ul>
				</li>
	          </ul>
	      </li>
	      
	      <li class="menu">
	          <ul>
			    <li class="button"><a href="#" class="orange">商品管理<span></span></a></li>          	
	
	            <li class="dropdown">
	                <ul>
	                    <li><a href="/d/item/add">导入商品</a></li>
	                    <li><a href="/d/item/manage">管理商品</a></li>
						 <li><a href="/d/category/add">添加商品类目</a></li>
	                </ul>
				</li>
	          </ul>
	      </li>
	 
	      <li class="menu">
	          <ul>
			    <li class="button"><a href="#" class="blue">卡券管理<span></span></a></li>
	
	            <li class="dropdown">
	                <ul>
	                    <li><a href="/d/card/manage">卡券管理</a></li>
	                </ul>
				</li>
	          </ul>
	      </li>
		  
		  <li class="menu">
	          <ul>
			    <li class="button"><a href="#" class="red">专题活动管理<span></span></a></li>
	
	            <li class="dropdown">
	                <ul>
	                    <li><a href="/d/activity/add">创建活动</a></li>
						<li><a href="/d/activity/manage">活动管理</a></li>
	                </ul>
				</li>
	          </ul>
	      </li>
	
	    
	      <li class="menu">
	          <ul>
			    <li class="button"><a href="#" class="green">其他<span></span></a></li>
	
	            <li class="dropdown">
	                <ul>
	                    <li><a href="/d/banner/manage">banner位管理</a></li>
	                </ul>
				</li>
	          </ul>
	      </li>
	  </ul>
	  
</div>	  
	  
	  
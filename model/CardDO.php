<?php
require_once 'BaseDO.php';

class CardDO extends BaseDO{
	
	public $id;
	
	public $source_id;
	
	public $name;
	
	public $card_type;
	
	public $shop_type;
	
	public $category_id;
	
	public $denomination;
	
	public $discount_rate;
	
	public $pic_url;
	
	public $money_condition;
	
	public $quantity_condition;
	
	public $merchant_id;
	
	public $merchant;
	
	public $limited;
	
	public $use_count;
	
	public $time_type;
	
	public $start;
	
	public $end;
	
	public $period;
	
	public $stocks;
	
	public $sold;
	
	public $like_count;
	
	public $status;
	
	public $last_modified;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}

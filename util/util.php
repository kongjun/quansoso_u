<?php

function getWaterFall($items,$numLine)
{
	$line = array();
	$len = count($items);

	if($len > 0)
	{
		$size = 0;
		if($len%$numLine==0)
		{
			$size=$len/$numLine;
		}else
		{
			$size= floor($len/$numLine)+1;
		}
		$pos=0;
		for ($i=1;$i<=$numLine;$i++)
		{
			$line2 = array();
			for ($j=1;$j<=$size;$j++)
			{
				$pos=($j-1)*$numLine+$i;
				if($pos>$len)
				{
					break;
				}
				$line2[] =$items[$pos-1];
			}
				
			$line[] = $line2;
		}
	}
	return $line;
}

function generateStr($length=6){
	
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$rand = "";
	for ( $i = 0; $i < $length; $i++ )
	{
		$rand .= substr($chars,mt_rand(0,strlen($chars)-1),1);
	}
	return $rand;
}

function getAutoPageArray($page,$pagelen){
	$pagecode = "";
	//页码计算范围
	$init = 1;
	$max = $page->totalPage;
	$pagelen = ($pagelen%2)?$pagelen:$pagelen+1;//页码个数
	$pageoffset = ($pagelen-1)/2;//页码个数左右偏移量

	//生成html
	//分页数大于页码个数时可以偏移
	if($page->totalPage > $pagelen)
	{
		//如果当前页小于等于左偏移
		if($page->currentPage<=$pageoffset)
		{
			$init=1;
			$max=$pagelen;
		}
		else
		{
			//如果当前页大于左偏移
			//如果当前页码右偏移超出最大分页数
			if($page->currentPage+$pageoffset>=$page->totalPage+1)
			{
				$init = $page->totalPage-$pagelen+1;
			}
			else
			{
				//左右偏移都存在时的计算
				$init = $page->currentPage-$pageoffset;
				$max = $page->currentPage+$pageoffset;

			}
		}
	}


	$array = array();
	for($i=$init;$i<=$max;$i++){
		$array[] = sprintf("%d",$i);
	}

	return $array;
}


function getIPAddress() {

	if (! empty ( $_SERVER ["HTTP_CLIENT_IP"] )) {
		$ip = $_SERVER ["HTTP_CLIENT_IP"];
	} elseif (! empty ( $_SERVER ["HTTP_X_FORWARDED_FOR"] )) {
		$ip = $_SERVER ["HTTP_X_FORWARDED_FOR"];
	} elseif (! empty ( $_SERVER ["REMOTE_ADDR"] )) {
		$ip = $_SERVER ["REMOTE_ADDR"];
	} else {
		$ip = "无法获取！";
	}
	return $ip;
}


function cutStringByStr($splitStr,$string,$star,$end){
	$arr = explode($splitStr, $string);
	return implode(".", array_slice($arr,$star,$end));
}





<?php
include '../../services/remoteService.php';
include '../../services/cardService.php';
include '../../services/merchantService.php';
include '../../services/topService.php';
include '../../dao/ShopCodeDao.php';

$lastModified = getMaxModified();
$bool = true;
$current = 1;
$num = 0;
$flage = -1;
while ($bool){
	
	$now = date("Y-m-d H:i:s",time());
	$resp = searchCouponCard("COUPON","ALL",$lastModified,"20","$current");
	$resp = json_decode($resp,true);
	$cards =  $resp['card_list'];
	for ($i=0,$len=count($cards);$i<$len;$i++){
	
		$card = $cards[$i];
		$cardExist = isExistCardBySourceId($card['card_id']);
		if($cardExist)
			continue;
		
		$cardDo = new CardDO($card);
		$cardDo->use_count = $card['useCount'];
		$cardDo->source_id = $card['card_id'];
		$discountRate = ($cardDo->money_condition-$cardDo->denomination)/$cardDo->money_condition*1000;
		$cardDo->discount_rate = intval($discountRate/100)*100;
		$cardDo->start = date("Y-m-d",strtotime($cardDo->start));
		$cardDo->end = date("Y-m-d",strtotime($cardDo->end));
		$cardDo->status = 1;
		//$cardDo->period = NULL;
		$cardDo->like_count = 0;
		$cardDo->gmt_created = $now;
		$cardDo->last_modified = $now;
		$bool = isExistMerchantByName($cardDo->merchant);
		if($bool){
			$merchant = getMerchantByName($cardDo->merchant);
			$cardDo->merchant_id = $merchant->id;
		}else {
			$merchantResp = searchMerchant($cardDo->merchant);
			$merchantResp = json_decode($merchantResp,true);
			$merchant = $merchantResp['merchant_list'][0];
			$merchantDo = new MerchantDO($merchant);
			$merchantDo->name = $cardDo->merchant;
			$merchantDo->external_shop_id = $merchant['shop_id'];
			$merchantDo->external_level = $merchant['shop_level'];
			$merchantDo->type = getShopNumByCode($merchant['shop_type']);
			$merchantDo->external_cid = $merchant['category_id'];
			$merchantDo->pic_url = $merchant['brand_pic_url'];
			$merchantDo->external_cid = $merchant['category_id'];
			$merchantDo->status = 1;
			$merchantDo->gmt_created = $now;
			$cardDo->merchant_id = addMerchant($merchantDo);
		}
		$num += addCard($cardDo);
	}
	$current++;
	$bool = $resp['has_next'];
	$flage++;
}

$callback = $_GET['callback'];

$resp = $callback.'({"result":';

if($flage>=0){
	$resp.= '"success","num":"'.$num.'"})';
}else{
	$resp.= '"error"})';
}
echo $resp;


/* $now = date("Y-m-d",time());
$resp1 = searchCouponCard("COUPON","all",$lastModified,"20","1");
$resp1 = json_decode($resp1,true);
$cards = $resp1['card_list'][0];
$cardDo = new CardDO($cards);
$cardDo->start = date("Y-m-d",strtotime($cardDo->start));
$cardDo->end = date("Y-m-d",strtotime($cardDo->end));
$cardDo->use_count = $cards['useCount'];
$cardDo->source_id = $cards['card_id'];
$discountRate = ($cardDo->money_condition-$cardDo->denomination)/$cardDo->money_condition*1000;
$cardDo->discount_rate = intval($discountRate/100)*100;
$cardDo->gmt_created = $now;
$cardDo->last_modified = $now;
$cardDo->merchant_id = 1;
//addCard($cardDo);
var_dump($cardDo); */

/* $name = "amh�ٷ��콢��";
$resp2 = searchMerchant ($name); 
$resp2 = json_decode($resp2,true);
$merchant = $resp2['merchant_list'][0];
$merchantDo = new MerchantDO($merchant);
$merchantDo->external_shop_id = $merchant['shop_id'];
$merchantDo->pic_url = $merchant['brand_pic_url'];
$merchantDo->external_cid = $merchant['category_id'];
var_dump($resp2); */

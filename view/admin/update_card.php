<?php
include '../../services/cardService.php';


$smarty = new Smarty ();

if(isset($_POST['name'])){
	
	$cardDo = new CardDO();
	$cardDo->id = $_POST['id'];
	$cardDo->name = $_POST['name'];
	$cardDo->pic_url = $_POST['pic_url'];
	$cardDo->start = $_POST['start'];
	$num = updateCardById($cardDo);
	$message = $num ? "更新成功！！！" : "更新失败！！！";
	$smarty->assign("message",$message);
}

if(isset($_REQUEST['id'])){

	$id = $_REQUEST['id'];
	$card = getCardById($id);
	$smarty->assign("card",$card);
}

$smarty->assign("menu","card");

$smarty->display ("templates/update_card.tpl");
<?php
include '../../services/activityService.php';

$current = isset($_GET['current']) ? intval($_GET['current']) : 1;
$pageSize = 10;

$page = getAllActivityByPage($current,$pageSize,"gmt_created");

$smarty = new Smarty ();

$smarty->assign("page",$page);
$smarty->assign("menu","activity_manager");

$smarty->display ("templates/activity_manage.tpl");
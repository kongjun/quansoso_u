<div class="quansoso_intaobao_warp"><!-- 头部 --> 
	{include file="../common/header.tpl"}
	
	<div style="clear: both"></div>	
	
	<div class="safe_area" style="background:#fff">
	
	<div class="pbwrap">
		<ul>
		<li style="height:185px" class="mglr15">
			<div class="option_menu" style="width:390px; height:152px;background:#efefef;float:left;padding:15px">
				<h1>卡券分类</h1>
				<ul id="yw0">
					<li class="cate_select"><a href="/d/coupons">全部</a></li>
					<li><a href="/d/coupons?category=1">女装</a></li>
					<li><a href="/d/coupons?category=2">美妆</a></li>
					<li><a href="/d/coupons?category=3">配饰</a></li>
					<li><a href="/d/coupons?category=4">男装</a></li>
					<li><a href="/d/coupons?category=5">鞋包</a></li>
					<li><a href="/d/coupons?category=6">运动</a></li>
					<li><a href="/d/coupons?category=7">食品</a></li>
					<li><a href="/d/coupons?category=9">数码</a></li>
					<li><a href="/d/coupons?category=8">其它</a></li>
				</ul>		

			<ul style="border-top:1px dashed #52515a;margin-top:10px">
					<li>
						<a href="/d/coupons?order=hot{if $category}&category={$category}{/if}">最热</a>
					</li>
					<li>
						<a href="/d/coupons?order=new{if $category}&category={$category}{/if}">最新</a>
					</li>
				</ul>
			</div>
			
		</li>
		{assign var="len" value=$page.resultList|@count}
		{foreach from=$page.resultList item=card name=foo}
		{if $smarty.foreach.foo.index >= $len/2}			   		 
			<li class="mglr15">
				 <div class="coupon_card_box">
			  		<a href="/d/coupon?id={$card.id}" target="_blank">
			  			<span class="coupon_cardlink_box">
				  			<div class="coupon_cardlink_bltop"></div>  		
				  			<!--<img id="http://img01.taobaocdn.com/imgextra/i1/729839245/T22K6CXc0XXXXXXXXX_!!729839245.jpg" src="http://img01.taobaocdn.com/imgextra/i1/729839245/T22K6CXc0XXXXXXXXX_!!729839245.jpg" style="visibility:hidden">-->
				  			<div class="coupon_cardlink_blbot"></div>
				  			<div class="cardimgbox">
				  				<img  src="{$card.pic_url}" >
				  			</div>
			  		    </span>
			  			<h3>{$card.merchant}</h3>
				  		<span class="coupon_value">
					  					<span class="priceval">{$card.denomination/100}</span><span class="pricfont">元优惠券</span>
				  		</span>
				  		<!--<span class="jgx_line">|</span>-->
				  		<span class="coupinfo_span">有效期至:<span style="margin-left:5px">{$card.end|date_format:"%Y-%m-%d"}</span></span>
				  	</a>	
			  	  </div>	
			  </li>
		  {/if}
		  {/foreach}
		  </ul>
		  
		 <!-- 右边栏 -->
		 <ul>
		 	{foreach from=$page.resultList item=card name=foo}
			{if $smarty.foreach.foo.index < $len/2}
				<li class="mglr15">
				 <div class="coupon_card_box">
			  		<a href="/d/coupon?id={$card.id}" target="_blank">
			  			<span class="coupon_cardlink_box">
				  			<div class="coupon_cardlink_bltop"></div>  						  			
				  			<div class="coupon_cardlink_blbot"></div>
				  			<div class="cardimgbox">
				  				<img  src="{$card.pic_url}" >
				  			</div>
			  		    </span>
			  			<h3>{$card.merchant}</h3>
				  		<span class="coupon_value">
					  					<span class="priceval">{$card.denomination/100}</span><span class="pricfont">元优惠券</span>
				  		</span>
				  		<!--<span class="jgx_line">|</span>-->
				  		<span class="coupinfo_span">有效期至:<span style="margin-left:5px">{$card.end|date_format:"%Y-%m-%d"}</span></span>
				  	</a>	
			  	  </div>	
			  </li>
		  {/if}
		  {/foreach}
		 </ul> 
		  			 
		</div>
		
		<div style="clear:both"></div>
		
		
		<div class="pagebtn">
			{if $page.currentPage > 1}
			<a href="/d/coupons?current={$page.currentPage-1}{if $order}&order={$order}{/if}{if $category}&category={$category}{/if}" class="pre" title="上一页"></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/coupons?current={$page.currentPage+1}{if $order}&order={$order}{/if}{if $category}&category={$category}{/if}" class="next" title="下一页"></a>
			{/if}
		</div>
		
		<div style="clear:both"></div>

	</div>

</div>

<script src="/assets/javascripts/front/card_list.js"></script>

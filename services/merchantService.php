<?php
include '../dao/MerchantDao.php';
include 'topService.php';


/**
 * 
 * @Description 获得商家信息通过nick
 * @param unknown $name
 * @return MerchantDO
 */
function getMerchantByName($nick){
	
	global $merchantDao;
	return $merchantDao->selectMerchantByName($nick);
}

/**
 * 
 * @Description 获得商家店铺热卖商品
 * @param unknown $sellerId
 * @param 要获得的商品数量 $count
 * @return multitype:Ambigous <Ambigous, unknown, mixed>
 */
/* function getMerchantRecommendItems($sellerId,$count){
	
	$items = array();
	while ($count > 0){
		$i = $count-10 > 0 ? 10 : $count;
		$items[] = getRecommendShopItems($sellerId,$i)->favorite_items->favorite_item;
		$count = $count - 10;
	}
	return $items;
} */

function getMerchantRecommendItems($sellerId,$count){
	
	$resp = getTaobaokeRelateItem($sellerId,$count);
	$items = array();
	return $resp->total_results > 0 ? $resp->taobaoke_items->taobaoke_item : $items;
}

/**
 * 
 * @Description 获得商家的UserId
 * @param unknown $keyword
 * @return number
 */
function getMerchantIdByKeyword($keyword){
	
	$resp = getTaobaokeShopByKeyword($keyword);
	return $resp->total_results> 0 ? $resp->taobaoke_shops->taobaoke_shop[0]->user_id : 0;
}



function addMerchant($merchant){
	
	global $merchantDao;
	return $merchantDao->insertMerchant($merchant);
}

/**
 * 
 * @Description 通过名商家称判断商家是否存在
 * @param unknown $name
 * @return boolean
 */
function isExistMerchantByName($name){
	
	global $merchantDao;
	$count = $merchantDao->selectCountMerchantByName($name);
	return $count > 0;
}

/**
 * 
 * @Description 更新商家的externalId
 * @param unknown $id
 * @param unknown $externalId
 */
function updateMerchantById($merchant){
	
	global $merchantDao;
	return $merchantDao->updateById($merchant);
}

/**
 * 
 * @Description 获得商家的店铺信息
 * @param unknown $nick
 * @return Ambigous <mixed, SimpleXMLElement, unknown>
 */
function getMerchantShopInfo($nick){
	
	try {
		
		return getShopInfo($nick)->shop;
		
	} catch (Exception $e) {
		
	}
}



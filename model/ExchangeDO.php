<?php
require_once ('BaseDO.php');

class ExchangeDO extends BaseDO{
	
	public $id;
	
	public $user_id;
	
	public $user_nick;
	
	public $card_id;
	
	public $title;
	
	public $get_type;
	
	public $card_type;
	
	public $channel;
	
	public $consume;
	
	public $consume_type;
	
	public $external_id;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}


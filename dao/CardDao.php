<?php
include '../model/CardDO.php';
include '../model/Page.php';

class CardDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description 通过名称查询卡券信息
	 * @param unknown $name
	 * @return CardDO
	 */
	public function selectCardByName($name){
		
		$sql = "select * from ekp_card where name = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($name));
		return new CardDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 查询卡券通过id
	 * @param unknown $id
	 * @return CardDO
	 */
	public function selectCardById($id){
		
		$sql = "select * from ekp_card where id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($id));
		return new CardDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 查询所有的卡券
	 * @param unknown $current
	 * @return Page
	 */
	public function selectAllCards($size,$current,$order){
		
		$sql = "select count(*) from ekp_card";
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page($size,$current,$totalCount);
		
		$sql = "select * from ekp_card order by $order desc limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($page->currentPage-1)*$page->pageSize,$page->pageSize));
		$cards = array();
		while (@$row=$exec->fetch()){
			
			$cards[] = new CardDO($row);
		}
		$page->resultList = $cards;
		return $page;
	}
	
	public function insertCard($card){
		
		$sql = "insert into ekp_card (source_id,name,card_type,shop_type,category_id,denomination,discount_rate,pic_url,money_condition,quantity_condition,merchant_id,merchant,limited,use_count,time_type,`start`,`end`,period,stocks,sold,like_count,`status`,last_modified,gmt_created) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($card->source_id,$card->name,$card->card_type,$card->shop_type,$card->category_id,$card->denomination,$card->discount_rate,$card->pic_url,$card->money_condition,$card->quantity_condition,$card->merchant_id,$card->merchant,$card->limited,$card->use_count,$card->time_type,$card->start,$card->end,$card->period,$card->stocks,$card->sold,$card->like_count,$card->status,$card->last_modified,$card->gmt_created));
	}
	
	public function selectMaxLastmodified(){
		
		$sql = "select max(last_modified) from ekp_card";
		return $this->pdo->query($sql)->fetchColumn();
	}
	
	/**
	 * 
	 * @Description 通过名称查询
	 * @param unknown $name
	 * @return Page
	 */
	public function selectLikeMerchantName($name){
		
		$sql = "select * from ekp_card where merchant like ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array("%".$name."%"));
		$page = new Page(10, 1, 1);
		$cards = array();
		while (@$row=$exec->fetch()){
			$cards[] =  new CardDO($row);
		}
		$page->resultList = $cards;
		return $page;
	}
	
	/**
	 * 
	 * @Description 通过sourceId查询卡券
	 * @param unknown $sourceId
	 * @return CardDO
	 */
	public function selectBySourceId($sourceId){
		
		$sql = "select * from ekp_card where source_id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($sourceId));
		return new CardDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 多条件查询--分页
	 * @param unknown $filter
	 * @param unknown $current
	 * @param unknown $size
	 * @param unknown $order
	 * @return Page
	 */
	public function selectPageByFilter($filter,$order,$current,$size){
		
		$sql = "select count(*) from ekp_card where 1=1 ".$filter;
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page($size, $current, $totalCount);
		
		$sql = "select * from ekp_card where 1=1 ".$filter." order by $order desc limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($current-1)*$size,$size));
		$cards = array();
		while (@$row=$exec->fetch()){
			$cards[] = new CardDO($row);
		}
		$page->resultList = $cards;
		return $page;
	}
	
	/**
	 * 
	 * @Description 更新状态
	 * @param unknown $id
	 * @param unknown $status
	 * @return boolean
	 */
	public function updateStatusById($id,$status){
		
		$sql = "update ekp_card set status = ? where id = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($status,$id));
	}
	
	/**
	 * 
	 * @Description 通过sourceId统计卡券
	 * @param unknown $id
	 * @return string
	 */
	public function selectCountBySourceId($sourceId){
		
		$sql = "select count(*) from ekp_card where source_id = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($sourceId));
		return $exec->fetchColumn();
	}
	
	/**
	 * 
	 * @Description 查询所有的卡券--条件过滤
	 * @param unknown $size
	 * @param unknown $current
	 * @param unknown $order
	 * @param unknown $filter
	 * @return Page
	 */
	public function selectAllCardsByFilter($size,$current,$order,$filter){
	
		$sql = "select count(*) from ekp_card where status = 1";
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page($size,$current,$totalCount);
	
		$sql = "select * from ekp_card where status = 1 $filter order by $order desc limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($page->currentPage-1)*$page->pageSize,$page->pageSize));
		$cards = array();
		while (@$row=$exec->fetch()){
				
			$cards[] = new CardDO($row);
		}
		$page->resultList = $cards;
		return $page;
	}
	
	/**
	 * 
	 * @Description 更新卡券
	 * @param unknown $card
	 * @return boolean
	 */
	public function updateById($card){
		
		$sql = "update ekp_card set name=?,pic_url=?,start=? where id = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($card->name,$card->pic_url,$card->start,$card->id));
	}
	
	/**
	 * 
	 * @Description 领用量降序查询
	 * @param unknown $size
	 * @return multitype:CardDO
	 */
	public function selectOrderSoldDescBySize($size){
		
		$sql = "select * from ekp_card order by sold desc limit 0,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($size));
		$cards = array();
		while (@$row=$exec->fetch()){
			$cards[] = new CardDO($row);
		}
		return $cards;
	}
	
	/**
	 * 
	 * @Description 通过商家获得卡券
	 * @param unknown $name
	 * @return multitype:CardDO
	 */
	public function selectByMerchantName($name){
		
		$sql = "select * from ekp_card where merchant = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($name));
		$cards = array();
		while (@$row=$exec->fetch()){
			$cards[] = new CardDO($row);
		}
		return $cards;
	}
	
	/**
	 * 
	 * @Description 更新卡券库存和领用量
	 * @param unknown $card
	 * @return boolean
	 */
	public function updateStockSoldBySourceId($card){
		
		$sql = "update ekp_card set stocks=?,sold=sold+1 where source_id=?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($card->stocks,$card->source_id));
	}
	
}

$cardDao = new CardDao();



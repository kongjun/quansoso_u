<?php
include '../../services/categoryService.php';
include '../../services/goodItemService.php';

$smarty = new Smarty ();

if(isset($_POST['items'])){
	
	/* $items = explode("\r\n", $_POST['items']);
	$itemIds = array();
	$categories = array();
	foreach ($items as $item){
		$iidAndCategory = split(' ', $item);
		$itemIds[] = $iidAndCategory[0];
		$categories[] = $iidAndCategory[1];
	}
	$itemIds = join(",", $itemIds);
	$num = 0;
	$num += batchAddItemsByIds($itemIds,$categories); */
	
	$items = explode("\r\n", $_POST['items']);
	$parentIds = $_POST['parent'];
	$categoryId = end($parentIds);
	$itemIds = join(",", $items);
	$num += batchAddItemsByIds($itemIds,$categoryId);
	$message = $num > 0 ? "成功添加：".$num."个！！！" : "添加失败！！！" ;
	$smarty->assign("message",$message);
}

$catgories = getCategoryByLevelNum(1);

$smarty->assign("catgories",$catgories);

$smarty->assign("menu","item_manager");

$smarty->display ("templates/add_item.tpl");
<?php
include '../dao/ActivityDao.php';

/**
 * 
 * @Description 添加活动
 * @param unknown $activityDo
 * @return boolean
 */
function addActivity($activityDo){
	
	global $activityDao;
	return $activityDao->insertActivity($activityDo);
}

/**
 * 
 * @Description 获得所有的活动---分页
 * @param unknown $current
 * @param unknown $size
 * @param unknown $order
 * @return Page
 */
function getAllActivityByPage($current, $size, $order){
	
	global $activityDao;
	return $activityDao->selectAllByPage($current, $size, $order);
}

/**
 * 
 * @Description 通过获得活动详情
 * @param unknown $id
 * @return ActivityDO
 */
function getActivityById($id){
	
	global $activityDao;
	return $activityDao->selectById($id);
}

/**
 *
 * @Description 通过获得活动详情
 * @param unknown $id
 * @return ActivityDO
 */
function getActivityNoContentById($id){

	global $activityDao;
	return $activityDao->selectNoContenById($id);
}

/**
 * 
 * @Description 更新活动
 * @param unknown $activityDo
 * @return boolean
 */
function updateActivityById($activityDo){
	
	global $activityDao;
	return $activityDao->updateById($activityDo);
}

/**
 * 
 * @Description 获得活动内容
 * @param unknown $merchant
 * @return ActivityDO
 */
function getActivityContentByMerchant($merchant){
	
	global $activityDao;
	return $activityDao->selectContentByMerchant($merchant);
}






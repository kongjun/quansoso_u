<?php
include '../dao/GoodItemDao.php';
include '../dao/ItemCodeDao.php';
include '../services/topService.php';
include 'cardService.php';
include 'merchantService.php';

/**
 * 
 * @Description 批量导入良品购商品
 * @param Array $goodItems
 * @return Integer
 */
function batchAddItemsByIds($ids,$categoryId){
	
	$num = 0;
	$now = date ( 'Y-m-d', time () );
	$code = getItemByCode("goodRecommend");
	$items = getListItemsInfo($ids); 
	global $goodItemDao;
	foreach ($items->items->item as $item){
		
		$goodItem = new GoodItemDO();
		//$goodItem->merchant_id = $item->ownerId;
		$goodItem->merchant_name = $item->nick;
		$goodItem->code = $code;
		$goodItem->iid = $item->num_iid;
		$goodItem->top_cid = $item->cid;
		//$categoryId = getCategoryByName($categories[$i])->id;
		$goodItem->ekp_cid = intval($categoryId);
		$goodItem->title = $item->title;
		$goodItem->price = intval($item->price)*100;
		//$goodItem->collected_count = $item->collectedCount;
		//$goodItem->sold_count = $item->soldCount;
		$goodItem->image_url = $item->pic_url;
		$goodItem->gmt_created = $now;
		$num += $goodItemDao->insertGoodItems($goodItem);
	}
	return $num;
}

/**
 * 
 * @Description 获得所有商品并分页
 * @param unknown $current
 * @param unknown $order
 * @return Page
 */
function getAllItem($current,$order){
	
	global $goodItemDao;
	return $goodItemDao->selectAllItems($current, $order);
}

/**
 * 
 * @Description 删除商品
 * @param unknown $id
 * @return boolean
 */
function deleteItemByIid($iid){
	
	global $goodItemDao;
	return $goodItemDao->deleteItemByIid($iid);
}

/**
 * 
 * @Description 通过名称获得商品
 * @param unknown $name
 * @return GoodItemDO
 */
function getPageItemLikeTitle($name){
	
	global $goodItemDao;
	return $goodItemDao->selectPageItemLikeTitle($name);
}

/**
 * 
 * @Description 通过类目查询商品---page分页
 * @param unknown $cids
 * @param unknown $pageSize
 * @param unknown $current
 * @return Page
 */
function getPageItemByCid($cids,$pageSize,$current){
	
	global $goodItemDao;
	return $goodItemDao->selectPageItemInEkpCid($cids,$pageSize,$current);
}

/**
 * 
 * @Description 获得商品对应的卡券
 * @param unknown $cids
 * @param unknown $pageSize
 * @param unknown $current
 * @return Page
 */
function getPageItemAndCouponCard($cids,$pageSize,$current){
	
	$page = getPageItemByCid($cids,$pageSize,$current);
	$items = array();
	foreach($page->resultList as $item){
		
		$newArr = array();
		$newArr['item'] = $item;
		$coupon = getCardByMerchantName($item->merchant_name);
		if(count($coupon)>0){
			$newArr['coupon'] = $coupon;
		}
		$items[] = $newArr;
	}
	$page->resultList = $items;
	return $page;
}

/**
 * 
 * @Description 更新商品
 * @param unknown $item
 * @return boolean
 */
function updateByItem($item){
	
	global $goodItemDao;
	return $goodItemDao->updateByItem($item);
}

/**
 * 
 * @Description 通过id获得商品
 * @param unknown $id
 * @return GoodItemDO
 */
function getItemByIid($iid){
	
	global $goodItemDao;
	return $goodItemDao->selectByIid($iid);
}



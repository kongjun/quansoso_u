
<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		<div class="admin_page">
			<form action="/d/item/manage" method="post" >
			<input type="text" name="title" class="admin_search_input" />&nbsp;<a><input type="submit" value="搜索" class="admin_save_button" /></a></a>
			
			{if $page.currentPage > 1}
			<a href="/d/item/manage?current={$page.currentPage-1}"><input type="button" value="上一页" class="admin_save_button" /></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/item/manage?current={$page.currentPage+1}"><input type="button" value="下一页" class="admin_save_button" /></a>
			{/if}
			<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
			</form>
		</div>
		
		<table border="1" class="admin_table">
		    <tr class="title">
		    	<td colspan="2">商品名称</td>
				<td>价格</td>
				<td>类目</td>
				<td width="80">创建时间</td>
				<td width="80">操作</td>
		    </tr>
			{foreach from=$items item=item name=foo}
			<tr class="sep-row"><td colspan="8"></td></tr>
			<tr class="order-hd">
				<td colspan="8">
					商品编号：<span class="admin_itemId">{$item.item.iid}</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					{$item.item.merchant_name}
				</td>
			</tr>
			<tr class="order-bd admin_flage_tr">
				<td class="baobei" colspan="2">
					<a target="_blank"  href="{$item.item.image_url}" class="pic">
						<img alt="{$item.item.title}" src="{$item.item.image_url}_310x310.jpg">
					</a>
					<div class="desc">
						<a class="baobei-name" target="_blank" href="http://item.taobao.com/item.htm?id={$item.item.iid}">
						<div class="spec">
							<span class="itemTitle">{$item.item.title}</span>
						</div>
						</a>
					</div>            
				</td>
				<td class="itemPrice">{$item.item.price/100} 元</td>
				<td class="itemCategory">{$item.category.name}</td>
				<td>{$item.item.gmt_created|date_format:"%Y-%m-%d"}</td>
				<td>
					<p><a><input type="button" value="修改" class="admin_save_button admin_update" /></a></p>
					<p><a><input type="button" value="删除" class="admin_save_button admin_delete" /></a></p>
				</td>
			</tr>
			{/foreach}
		</table>
		<div class="admin_page">
			{if $page.currentPage > 1}
			<a href="/d/item/manage?current={$page.currentPage-1}"><input type="button" value="上一页" class="admin_save_button" /></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/item/manage?current={$page.currentPage+1}"><input type="button" value="下一页" class="admin_save_button" /></a>
			{/if}
			<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
		</div>
		
	</div>
	
</div>



<div class="admin_outer"></div>
<div class="admin_inner admin_white_content" style="display:none">
		<p style="float:right;margin-top:-10px;margin-right:-10px;"><a href="#" class="admin_close">关闭<a></p>
	    <table height="100" class="updateTable">
	    	<tr>
				<td align="right">商品名称: </td><td><input type="text" class="admin_input admin_name" /></td>
	    	</tr>
			<tr>
				<td align="right"></td>
				<td><span class="admin_warn" style="color:red"></span></td>
	    	</tr>
	    	<tr>
				<td align="right">商品图片: </td><td><input type="text" class="admin_input admin_imgUrl" /></td>
	    	</tr>
			<tr>
				<td align="right">商品类目: </td><td><input type="text" class="admin_input admin_category" /></td>
	    	</tr>
			<tr>
				<td colspan="2" align="center"><a><input type="button" value="确定" class="admin_button admin_save_button" /></a></td>
	    	</tr>
		</table>
</div>

<script src="/assets/javascripts/admin/item_manage.js"></script>

<?php
include '../../services/goodItemService.php';
include '../../services/categoryService.php';


$callback = $_GET['callback'];
$iid = $_GET['iid'];
$name = urldecode_utf8($_GET['name']);
$category = urldecode_utf8($_GET['category']);
$imgUrl = $_GET['imgUrl'];

$itemDO = new GoodItemDO();
$itemDO->iid = $iid;
$itemDO->title = $name;
$itemDO->image_url = $imgUrl;
$itemDO->ekp_cid = getCategoryByName($category)->id;

$num = updateByItem($itemDO);
$resp = $callback.'({"result":';
if($num){
	$resp.= '"success"})';
}else{
	$resp.= '"error"})';
}

echo $resp;

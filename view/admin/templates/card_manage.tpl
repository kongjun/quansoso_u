
<div class="admin_updateDate">
	上次更新时间：{$lastModified}
</div>	

<div class="admin_wrap">
	
	<div class="admin_left">
		{include file="../common/menu.tpl"}
	</div>
	
	<div class="admin_right">
		<div class="admin_page">
			<form action="/d/card/manage" method="post" >
			<input type="text" name="name" class="admin_search_input" />&nbsp;<input type="submit" value="搜索" class="admin_save_button" /></a>
			
			<input type="button" value="同步卡券库" class="admin_cardSyn admin_save_button" />
			<a href="/d/card/manage?order=denomination{if $status}&status={$status}{/if}{if $shopType}&shopType={$shopType}{/if}{if $denomination}&denomination={$denomination}{/if}"><input type="button" value="面值降序" class="{if $order=="denomination"}admin_save_button_red{else}admin_save_button{/if}" /></a>
			<a href="/d/card/manage?order=sold{if $status}&status={$status}{/if}{if $shopType}&shopType={$shopType}{/if}{if $denomination}&denomination={$denomination}{/if}"><input type="button" value="领取量降序" class="{if $order=="sold"}admin_save_button_red{else}admin_save_button{/if}" /></a>
			{if $page.currentPage > 1}
			<a href="/d/card/manage?current={$page.currentPage-1}{if $order}&order={$order}{/if}{if $status}&status={$status}{/if}{if $shopType}&shopType={$shopType}{/if}{if $denomination}&denomination={$denomination}{/if}"><input type="button" value="上一页" class="admin_save_button" /></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/card/manage?current={$page.currentPage+1}{if $order}&order={$order}{/if}{if $status}&status={$status}{/if}{if $shopType}&shopType={$shopType}{/if}{if $denomination}&denomination={$denomination}{/if}"><input type="button" value="下一页" class="admin_save_button" /></a>
			{/if}
			<span>&nbsp;&nbsp;&nbsp;共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
			</form>
			<div class="admin_filter">
				<form action="/d/card/manage" method="post">
				状态：
				<select name="status">
					<option value=""></option>
					<option value="1" {if $status==1}selected=selected{/if}>已上架</option>
					<option value="0"  {if $status==0 && $status}selected=selected{/if}>已下架</option>
					<option value="3" {if $status==3}selected=selected{/if}>已过期</option>
				</select>
				店铺类型：
				<select name="shopType">
					<option value=""></option>
					<option value="1" {if $shopType==1}selected=selected{/if}>淘宝C店</option>
					<option value="2" {if $shopType==2}selected=selected{/if}>天猫</option>
				</select>
				面值：
				<input type="checkbox" name="denomination[]" value="500" class="admin_denomination_checkbox"> 5元&nbsp;
				<input type="checkbox" name="denomination[]" value="1000" class="admin_denomination_checkbox"> 10元&nbsp;
				<input type="checkbox" name="denomination[]" value="2000" class="admin_denomination_checkbox"> 20元&nbsp;
				<input type="checkbox" name="denomination[]" value="5000" class="admin_denomination_checkbox"> 50元&nbsp;
				<input type="checkbox" name="denomination[]" value="10000" class="admin_denomination_checkbox"> 100元&nbsp;
				<input type="submit" value="查询" class="admin_save_button" />
				<input type="hidden" value="{$denomination}" class="admin_denominations" />
				</form>
			</div>
		</div>
		<table border="1" class="admin_table">
	    <tr class="title">
	    	<td colspan="2">卡券名称</td>
			<td>店铺类型</td>
			<td>面值</td>
			<td width="80">开始时间</td>
			<td width="80">结束时间</td>
			<td>总发行量</td>
			<td>已领取量</td>
			<td>状态</td>
			<td>操作</td>
	    </tr>
		{foreach from=$page.resultList item=card name=foo}
		<tr class="sep-row"><td colspan="10"></td></tr>
		<tr class="order-hd"><td colspan="10">卡券编号：{$card.source_id}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$card.merchant}</td></tr>
		<tr class="order-bd">
			<td class="baobei" colspan="2">
				<a target="_blank"  href="{$card.pic_url}" class="pic">
					<img alt="{$card.name}" src="{$card.pic_url}_310x310.jpg">
				</a>
				<div class="desc">
					<a class="baobei-name" target="_blank" href="/d/coupon?id={$card.id}">
					<div class="spec">
						<span>{$card.name}</span>
					</div>
					</a>
				</div>            
			</td>
			<td>
				{if $card.shop_type==2}
					天猫
				{else}
					淘宝C店	
				{/if}
			</td>
			<td>{$card.denomination/100} 元</td>
			<td>{$card.start|date_format:"%Y-%m-%d"}</td>
			<td>{$card.end|date_format:"%Y-%m-%d"}</td>
			<td>{$card.stocks}</td>
			<td>{$card.sold}</td>
			<td>
				{if $card.end < $now}
					已过期
				{elseif $card.status==1}
					已上架
				{elseif $card.status==0}
					已下架
				{/if}
			</td>
			<td>
				{if $card.end > $now}
					{if $card.status==1}
						<p>
							<a><input type="button" value="下架" class="admin_save_button_red admin_cardDown" /></a>
							<input type="hidden" value="{$card.id}" class="admin_down_id"  />
						</p>
					{elseif $card.status==0}
						<p>
							<a><input type="button" value="上架" class="admin_save_button admin_cardUp" /></a>
							<input type="hidden" value="{$card.id}" class="admin_up_id"  />
						</p>
					{/if}
					<p><a href="/d/card/update?id={$card.id}"><input type="button" value="修改" class="admin_save_button admin_update" /></a></p>
				{/if}
				
			</td>
		</tr>
		{/foreach}
	</table>
	<div class="admin_page">
		{if $page.currentPage > 1}
		<a href="/d/card/manage?current={$page.currentPage-1}{if $order}&order={$order}{/if}{if $status}&status={$status}{/if}{if $shopType}&shopType={$shopType}{/if}{if $denomination}&denomination={$denomination}{/if}"><input type="button" value="上一页" class="admin_save_button" /></a>
		{/if}
		{if $page.currentPage < $page.totalPage}
		<a href="/d/card/manage?current={$page.currentPage+1}{if $order}&order={$order}{/if}{if $status}&status={$status}{/if}{if $shopType}&shopType={$shopType}{/if}{if $denomination}&denomination={$denomination}{/if}"><input type="button" value="下一页" class="admin_save_button" /></a>
		{/if}
		<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
	</div>
		
	</div>
	
</div>

<div class="admin_outer">
	 <div class="admin_inner admin_white_content">
	 	<span style="float:right;margin-top:-10px;margin-right:-10px;cursor:pointer" class="admin_close"><a href="/d/card/manage">关闭</a></span>
       	<p style="text-align:center;line-height:100px;" ><span class="message">正在请求，请等待。。。</span></p>
    </div>
</div>

<script src="/assets/javascripts/admin/card_manage.js"></script>
<?php
include '../dao/CardDao.php';

/**
 * 
 * @Description 获得卡券ByName
 * @param unknown $name
 * @return CardDO
 */
function getCardByName($name){
	
	global $cardDao;
	return $cardDao->selectCardByName($name);
}

/**
 * 
 * @Description 获得卡券ById
 * @param unknown $id
 * @return CardDO
 */
function getCardById($id){
	
	global $cardDao;
	return $cardDao->selectCardById($id);
}

/**
 * 
 * @Description 获得所有的卡券
 * @param unknown $current
 * @return Page
 */
function getAllCard($size,$current,$order){
	
	global $cardDao;
	return $cardDao->selectAllCards($size,$current,$order);
}

function addCard($card){
	
	global $cardDao;
	return $cardDao->insertCard($card);
}


function getMaxModified(){
	
	global $cardDao;
	return $cardDao->selectMaxLastmodified();
}


function getPageCardLikeMerchantName($name){
	
	global $cardDao;
	return $cardDao->selectLikeMerchantName($name);
}

/**
 * 
 * @Description Enter 通过sourceId获得卡券
 * @param unknown $sourceId
 * @return CardDO
 */
function getCardBySourceId($sourceId){
	
	global $cardDao;
	return $cardDao->selectBySourceId($sourceId);
}

/**
 * 
 * @Description 条件筛选
 * @param unknown $filter
 * @param unknown $current
 * @param unknown $size
 * @param unknown $order
 * @return Page
 */
function getCardPageByFilter($filter,$order,$current,$size){
	
	global $cardDao;
	return $cardDao->selectPageByFilter($filter,$order,$current,$size);
}

/**
 * 
 * @Description 更新状态
 * @param unknown $id
 * @param unknown $status
 * @return boolean
 */
function updateStatusById($id, $status){
	
	global $cardDao;
	return $cardDao->updateStatusById($id,$status);
}

/**
 * 
 * @Description 判断卡券是否存在
 * @param unknown $sourceId
 * @return boolean
 */
function isExistCardBySourceId($sourceId){
	
	global $cardDao;
	$count = $cardDao->selectCountBySourceId($sourceId);
	return $count==0 ? false : true;
}

/**
 * 
 * @Description 获得所有卡券--条件过滤
 * @param unknown $size
 * @param unknown $current
 * @param unknown $order
 * @param unknown $filter
 * @return Page
 */
function getAllCardByFilter($size,$current,$order,$filter){
	
	global $cardDao;
	return $cardDao->selectAllCardsByFilter($size, $current, $order, $filter);
}

/**
 * 
 * @Description 更新卡券
 * @param unknown $card
 * @return boolean
 */
function updateCardById($card){
	
	global $cardDao;
	return $cardDao->updateById($card);
}

/**
 * 
 * @Description 获得热券推荐
 * @param unknown $size
 * @return Ambigous <multitype:CardDO, multitype:CardDO >
 */
function getHotCardBySize($size){
	
	global $cardDao;
	return $cardDao->selectOrderSoldDescBySize($size);
}

/**
 * 
 * @Description 获得商家的卡券
 * @param unknown $name
 * @return Ambigous <multitype:CardDO, multitype:CardDO >
 */
function getCardByMerchantName($name){
	
	global $cardDao;
	return $cardDao->selectByMerchantName($name);
}

/**
 * 
 * @Description 领券之后更新卡券
 * @param unknown $card
 * @return boolean
 */
function updateCardAfterSold($card){
	global $cardDao;
	return $cardDao->updateStockSoldBySourceId($card);
}





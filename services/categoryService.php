<?php

include("../dao/CategoryDao.php");

/**
 * 
 * @Description 通过名称获得类目
 * @param unknown $name
 * @return CategoryDO
 */
function getCategoryByName($name)
{
	global $categoryDao;
	return $categoryDao->selectCategoryByName($name);
}

/**
 * 
 * @Description 获得类目菜单
 * @param unknown $menuLength
 * @return Ambigous <multitype:CategoryDO, multitype:CategoryDO >
 */
function getCategoryByIsMenu($menuLength){
	
	global $categoryDao;
	return $categoryDao->selectCategoryByIsMenu($menuLength);
}

/**
 * 
 * @Description 通过id获得类目
 * @param unknown $categoryId
 * @return CategoryDO
 */
function getCategoryById($categoryId){
	
	global $categoryDao;
	return $categoryDao->selectById($categoryId);
}

/**
 * 
 * @Description 通过id获得旗下的子类目数组
 * @param unknown $categoryId
 * @return multitype:array
 */
function getSunCategoryIdsByLeveCode($levelCode){
	
	global $categoryDao;
	$sunCategorys = $categoryDao->selectSunByCode($levelCode);
	$cids = array();
	foreach($sunCategorys as $sunCategory){
		
		$cids[] = $sunCategory->id;
	}
	return $cids;
}

/**
 * 
 * @Description 获得一个类目下的所有类目
 * @param unknown $levelCode
 * @param unknown $levelNum
 * @param unknown $isShowTag
 * @return Ambigous <multitype:CategoryDO, multitype:CategoryDO >
 */
function getCategoryTagByLevelCode($levelCode,$levelNum,$isShowTag){
	
	global $categoryDao;
	return $categoryDao->selectCategoryLikeLevelCode($levelCode, $levelNum, $isShowTag);
}

/**
 * 
 * @Description 获得该类目下最大的子类目
 * @param unknown $parentId
 * @return CategoryDO
 */
function getMaxSunCategoryByParentId($parentId){
	
	global $categoryDao;
	return $categoryDao->selectMaxSunByParentId($parentId);
}

/**
 * 
 * @Description 添加类目
 * @param unknown $category
 * @return boolean
 */
function addCategory($category){
	
	global $categoryDao;
	return $categoryDao->insertCategory($category);
}

/**
 * 
 * @Description 通过levelNum获得类目
 * @param unknown $levelNum
 * @return Ambigous <multitype:CategoryDO, multitype:CategoryDO >
 */
function getCategoryByLevelNum($levelNum){
	
	global $categoryDao;
	return $categoryDao->selectByLevelNum($levelNum);
}

/**
 * 
 * @Description 获得子类目
 * @param unknown $parentId
 * @return Ambigous <multitype:CategoryDO, multitype:CategoryDO >
 */
functIon getSunCategoryByParenId($parentId){
	
	global $categoryDao;
	return $categoryDao->selectSunByParentId($parentId);
}







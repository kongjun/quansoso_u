<?php
include '../../services/categoryService.php';
include '../../services/goodItemService.php';
include '../../services/bannerService.php';
include '../../util/util.php';

//Menu
$randArr = array(3,4,5,6,7,8);
$rand = $randArr[array_rand($randArr)];
$categoryId = isset($_GET ['category']) ? intval($_GET ['category']) : $rand;
$categorys = getCategoryByIsMenu(6);


$current = isset($_GET['current']) ? intval($_GET['current']) : 1;


$currentCategory = getCategoryById($categoryId);
//标签部分
$menuLevelCode = cutStringByStr(".",$currentCategory->level_code,0,2);
$tagCategory = getCategoryTagByLevelCode($menuLevelCode,2,1);


//瀑布流
$cidArr = getSunCategoryIdsByLeveCode($currentCategory->level_code);
$page = getPageItemAndCouponCard(implode(",", $cidArr),50,$current);
$line = getWaterFall($page->resultList,4);

//分页
$pageArray = getAutoPageArray($page,5);

//banner位
$menuId = isset($_GET ['menu']) ? intval($_GET ['menu']) : $rand-2;
$banner = getBannerByCode("TAOBAOBEI-".$menuId)[0];

$smarty = new Smarty ();
$smarty->assign("categories",$categorys);
$smarty->assign("menu",$menuId);
$smarty->assign("tagCategory",$tagCategory);
$smarty->assign("categoryId",$categoryId);
$smarty->assign ("line", $line);
$smarty->assign ("page", $page);
$smarty->assign ("pageArray", $pageArray);
$smarty->assign("banner",$banner);
$smarty->assign("mark","goodRecommend");

$smarty->display ( "templates/goodlist.tpl");
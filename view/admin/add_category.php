<?php
include '../../services/categoryService.php';

$smarty = new Smarty ();

if(isset($_POST['names'])){
	
	$names = $_POST['names'];
	$names = explode ( "\r\n", $names);
	$parentIds = $_POST['parent'];
	$parentId = end($parentIds);
	if($parentId==""){
		array_pop($parentIds);
		$parentId = end($parentIds);
	}
	$i = 0;
	foreach ( $names as $name ){
		
		$catgory = new CategoryDO ();
		$catgory->name = trim ( $name );
		$catgory->gmt_created = date ( "Y-m-d H:i:s", time () );
		if (empty($parentId)){
			$toplevel = getMaxSunCategoryByParentId (0);
			$nextLevel = sprintf ( "%03d", $toplevel->level_code + 1 );
			$catgory->parent_id = 0;
			$catgory->level_code = $nextLevel;
			$catgory->level_num = 1;
		}
		else{
			$parent = getCategoryById($parentId);
			$maxSun = getMaxSunCategoryByParentId($parent->id);
			$nextLevel = strtok ( $maxSun->level_code, "." );
			$str = $nextLevel;
			while ( $str !== false ){
				$str = strtok ( "." );
				if ($str !== false){
					$nextLevel = $str;
				}
			}
			if ($nextLevel){
				$nextLevel = sprintf ( "%03d", $nextLevel + 1 );
			}
			else{
				$nextLevel = "001";
			}
			$nextLevel = $parent->level_code . "." . $nextLevel;
			$catgory->level_code = $nextLevel;
			$catgory->parent_id = $parent->id;
			$catgory->level_num = count ( explode ( ".", $nextLevel ) );
		}
		$num = addCategory ($catgory);
		if($num >0 ){
			$i++;
		}
	}
	
	$count = count($names);
	$message = $count == $i ? "添加类目成功,共添加类目".$i."个！！！" : "添加类目失败,成功添加类目".$i."个，失败".$count-$i."个！！！";
	$smarty->assign ("message",$message);
}

$catgories = getCategoryByLevelNum(1);

$smarty->assign("catgories",$catgories);
$smarty->assign("menu","item_manager");

$smarty->display ("templates/add_category.tpl");



<div class="admin_wrap">

	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>

	<div class="admin_right">
		
		{include file="../common/newMessage.tpl"}
	
		<div  class="admin_tab">
			
		    <ul class="admin_tab_nav">
			
		        <li class="admin_tab_active admin_tab_title">模板A</li>
			
		        <li class="admin_tab_title">模板B</li>
			
		        <li class="admin_tab_title">模板 C</li>
			
		        <li class="admin_tab_title">模板 D</li>
			
		        <li class="admin_tab_title">模板 E</li>
			
		    </ul>
			
		    <div class="admin_tab_content">
			
			{foreach from=$cmses item=cms name=foo}
			<input type="hidden" class="admin_cms_id" value="{$cms.id}" />
		        <div class="admin_tab_subContent" {if $smarty.foreach.foo.index!=0}style="display:none"{/if}>

					<div class="admin_content">
						
						{$cms.content}	
			
					</div>
				
				</div>
			{/foreach}
		    </div>
			
			<div class="admin_save_back">
				<!--
				<form action="/d/cms/coupon" method="post">
					<input type="hidden" name="content_html" class="content_html" />
					<a><input type="button" class="admin_save_button admin_preview" value="预   览"></a>
					<a><input type="submit" class="admin_save_button admin_save" value="保   存"></a>
				</form>
				-->
				<form action="/d/cms/coupon" method="post">
					<input type="hidden" name="content_html" class="content_html" />
					<input type="hidden" name="id" class="J_cms_id" />
					<input type="hidden" name="templateId" class="J_cms_templateId" />
					<a><input type="submit" class="admin_save_button admin_save" value="更   新"></a>
				</form>
			</div>
			
		</div>
	</div>

</div>

<div class="admin_outer">
    <div class="admin_innner admin_white_content">
    	<p style="float:right;margin-top:-10px;margin-right:-10px;"><a href="#" class="admin_close">关闭</a></p>
        <table height="150" align="center">
        	<tr>
				<td align="left">格式: 优惠券编号 优惠券图片</td>
        	</tr>
			<tr>
				<td align="left">说明：共两个参数，每个参数以空格隔开</td>
        	</tr>
        	<tr>
				<td align="right">请输入参数: <input type="text" class="admin_input admin_param" /></td>
        	</tr>
			<tr>
				<td align="right" class="message"></td>
        	</tr>
			<tr>
				<td colspan="2" align="center"><a><input type="button" value="确定" class="admin_button admin_save_button" /></a></td>
        	</tr>
		</table>	
    </div>
</div>

<div class="admin_preview_outer"></div>
<div class="admin_preview_inner">
	<p style="float:right;margin-top:-10px;margin-right:-10px;"><a href="#" class="admin_preview_close">关闭</a></p>
	<div class="admin_preview_html"></div>
</div>

<script src="/assets/javascripts/admin/cms_coupon.js"></script>

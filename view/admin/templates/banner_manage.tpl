
<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		
		<div class="admin_fadeOut_message"></div>
		
		{if $page.currentPage > 1}
		<a href="/d/banner/manage?current={$page.currentPage-1}"><input type="button" value="上一页" class="admin_save_button" /></a>
		{/if}
		{if $page.currentPage < $page.totalPage}
		<a href="/d/banner/manage?current={$page.currentPage+1}"><input type="button" value="下一页" class="admin_save_button" /></a>
		{/if}
		<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
		
		<table border="1" class="admin_table">
		    <tr class="title">
		    	<td colspan="2">图片</td>
				<td width="80">信息</td>
				<td width="80">位置</td>
				<td width="180">备注</td>
				<td width="80">操作</td>
		    </tr>
			{foreach from=$page.resultList item=banner}
			<tr class="sep-row"><td colspan="8"></td></tr>
			<tr class="order-hd">
				<td colspan="8">
					&nbsp;&nbsp;编号：<span class="admin_itemId">{$banner.banner.id}</span>
				</td>
			</tr>
			<tr class="order-bd admin_flage_tr">
				<td class="baobei" colspan="2">
					<a target="_blank"  href="{$banner.banner.img_url}" class="pic">
						<img alt="{$banner.banner.title}" src="{$banner.banner.img_url}" class="admin_itemImg">
					</a>
					<div class="desc">
						<a class="baobei-name admin_itemLink" target="_blank" href="{$banner.banner.link}">
						<div class="spec">
							<span class="admin_itemTitle">{$banner.banner.title|default:""}</span>
						</div>
						</a>
					</div>            
				</td>
				<td class="admin_itemDesc">{$banner.banner.describe}</td>
				<td>{$banner.position}</td>
				<td>{$banner.banner.memo|default:""}</td>
				<td>
					<p><a href=""><input type="button" value="替换" class="admin_save_button admin_replace" /></a></p>
				</td>
			</tr>
			{/foreach}
		</table>
		
		<div class="admin_page">
			
			{if $page.currentPage > 1}
			<a href="/d/banner/manage?current={$page.currentPage-1}"><input type="button" value="上一页" class="admin_save_button" /></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/banner/manage?current={$page.currentPage+1}"><input type="button" value="下一页" class="admin_save_button" /></a>
			{/if}
			<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
		</div>
		
		
	</div>
	
</div>

<div class="admin_alertBox">
	<div class="admin_alertBox_outer"></div>
	<div class="admin_alertBox_inner">
			<p style="float:right;margin-top:-10px;margin-right:-10px;"><a href="#" class="admin_close">关闭<a></p>
		    <table height="150" class="updateTable">
		    	<tr>
					<td align="right">图片地址: </td><td><input type="text" class="admin_input admin_imgUrl" /></td>
		    	</tr>
		    	<tr>
					<td align="right">标题: </td><td><input type="text" class="admin_input admin_title" /></td>
		    	</tr>
				<tr>
					<td align="right"></td>
					<td><span class="admin_warn" style="color:red"></span></td>
		    	</tr>
				<tr>
					<td align="right">信息: </td><td><input type="text" class="admin_input admin_describe" /></td>
		    	</tr>
				<tr>
					<td align="right">跳转地址: </td><td>http://<input type="text" class="admin_input admin_link" /></td>
		    	</tr>
				<tr>
					<td colspan="2" align="center"><a><input type="button" value="确定" class="admin_button admin_save_button" /></a></td>
		    	</tr>
			</table>
	</div>
</div>


<script src="/assets/javascripts/admin/banner_manage.js"></script>

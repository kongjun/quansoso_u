<?php
class CouponSearchRequest {
	
	private $nick;
	
	private $ip;
	
	private $keywords;
	
	private $cardType;
	
	private $sort;
	
	private $pageSize;
	
	private $currentPage;
	
	private $highLight;
	
	public function getParams() {
		$params = array ();
		$params ['service'] = $this->getApiMethodName ();
		$params ['nick'] = $this->nick;
		$params ['ip'] = $this->ip;
		$params ['keywords'] = utf8_encode($this->keywords);
		$params ['cardType'] = $this->cardType;
		$params ['sort'] = $this->sort;
		$params ['pageSize'] = $this->pageSize;
		$params ['currentPage'] = $this->currentPage;
		$params ['highLight'] = $this->highLight;
		return $params;
	}
	
	public function getApiMethodName() {
		return "quansoso_web_search_card";
	}
	/**
	 *
	 * @return the $ip
	 */
	public function getIp() {
		return $this->ip;
	}
	
	/**
	 *
	 * @param field_type $ip        	
	 */
	public function setIp($ip) {
		$this->ip = $ip;
	}
	
	/**
	 *
	 * @return the $keywords
	 */
	public function getKeywords() {
		return $this->keywords;
	}
	
	/**
	 *
	 * @return the $cardType
	 */
	public function getCardType() {
		return $this->cardType;
	}
	
	/**
	 *
	 * @return the $sort
	 */
	public function getSort() {
		return $this->sort;
	}
	
	/**
	 *
	 * @return the $pageSize
	 */
	public function getPageSize() {
		return $this->pageSize;
	}
	
	/**
	 *
	 * @return the $nick
	 */
	public function getNick() {
		return $this->nick;
	}
	
	/**
	 *
	 * @param field_type $nick        	
	 */
	public function setNick($nick) {
		$this->nick = $nick;
	}
	
	/**
	 *
	 * @return the $currentPage
	 */
	public function getCurrentPage() {
		return $this->currentPage;
	}
	
	/**
	 *
	 * @return the $highLight
	 */
	public function getHighLight() {
		return $this->highLight;
	}
	
	/**
	 *
	 * @param field_type $keywords        	
	 */
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}
	
	/**
	 *
	 * @param field_type $cardType        	
	 */
	public function setCardType($cardType) {
		$this->cardType = $cardType;
	}
	
	/**
	 *
	 * @param field_type $sort        	
	 */
	public function setSort($sort) {
		$this->sort = $sort;
	}
	
	/**
	 *
	 * @param field_type $pageSize        	
	 */
	public function setPageSize($pageSize) {
		$this->pageSize = $pageSize;
	}
	
	/**
	 *
	 * @param field_type $currentPage        	
	 */
	public function setCurrentPage($currentPage) {
		$this->currentPage = $currentPage;
	}
	
	/**
	 *
	 * @param field_type $highLight        	
	 */
	public function setHighLight($highLight) {
		$this->highLight = $highLight;
	}
}


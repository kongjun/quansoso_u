<?php
require_once 'BaseDO.php';

class GoodItemDO extends BaseDO{
	
	public $id;
	
	public $merchant_name;
	
	public $merchant_id;
	
	public $top_cid;
	
	public $ekp_cid;
	
	public $iid;
	
	public $title;
	
	public $image_url;
	
	public $price;
	
	public $sold_count;
	
	public $collected_count;
	
	public $code;
	
	public $memo;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}

<?php
require_once 'BaseDO.php';

class ActivityDO extends BaseDO {
	
	public $id;
	
	public $title;
	
	public $headline;
	
	public $merchant;
	
	public $img_url;
	
	public $begin;
	
	public $end;
	
	public $explain;
	
	public $content;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}


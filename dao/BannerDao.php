<?php
include '../model/BannerDO.php';
include '../model/Page.php';

class BannerDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description 查询所有banner---分页
	 * @param unknown $current
	 * @param unknown $size
	 * @return Page
	 */
	public function selectPageAll($current,$size){
		
		$sql = "select count(*) from ekp_banner";
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page($size,$current,$totalCount);
		
		$sql = "select * from ekp_banner limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($current-1)*$size,$size));
		$banners = array();
		while (@$row=$exec->fetch()){
			$banners[] = new BannerDO($row);
		}
		$page->resultList = $banners;
		return $page;
		
	}
	
	/**
	 * 
	 * @Description 更新
	 * @param unknown $banner
	 * @return boolean
	 */
	public function updateById($banner){
		
		$sql = "update ekp_banner set img_url=?,title=?,`describe`=?,link=?,gmt_created=? where id = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($banner->img_url,$banner->title,$banner->describe,$banner->link,$banner->gmt_created,$banner->id));
	}
	
	/**
	 * 
	 * @Description 通过位置查询
	 * @param unknown $position
	 * @return multitype:BannerDO
	 */
	public function selectByPosition($position){
		
		$sql = "select * from ekp_banner where position = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($position));
		$banners = array();
		while (@$row=$exec->fetch()){
			$banners[] = new BannerDO($row);
		}
		return $banners;
	}
	
	
}

$bannerDao = new BannerDao();
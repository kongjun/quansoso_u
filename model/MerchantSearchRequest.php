<?php
class MerchantSearchRequest {
	
	public $systemParam;
	
	public $shop_type;
	
	public $merchant;
	
	public $shop_level;
	
	public $category_ids;
	
	public $last_modified;
	
}

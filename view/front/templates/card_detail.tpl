
<!-- login tip -->
<div class="logintip"> 
	<a class="close-box"></a>
	<div class="ks-stdmod-body">
			<div style="margin-top:25px;margin-left:25px">
				<i class="logintipi"></i>
				<div class="rule">亲 ，需要登录后才能兑换优惠券</div>
			</div>
			<div class="tologin" style="margin-top:24px">
				<a href="/d/login?id={$card.id}">登录</a>
			</div>
	</div>
</div>

<!-- success  tip -->
<div class="lqdiv"> 
	<a class="close-box"></a>
	<div class="ks-stdmod-body">
			<div style="margin-top:25px;margin-left:25px">
				<i class="successi"></i>
				<div class="rule">
					亲 ，您已兑换成功										
				</div>
			</div>
			<div class="btn-close" style="margin-top:24px">
				
			</div>
	</div>
</div>

<!-- callback tip -->
<div class="lqcallbacktip"> 
	<a class="close-box"></a>
	<div class="ks-stdmod-body">
			<div style="margin-top:25px;margin-left:25px">
				<i class="logintipi"></i>
				<div class="rule">
					<span class="J_callback"></span>										
				</div>
			</div>
			<div class="btn-close" style="margin-top:24px">
				<a></a>
			</div>
	</div>
</div> 


<input type="hidden" value="{$card.source_id}" class="J_couponId" />
<div class="quansoso_intaobao_warp">
	<!-- 头部 -->
	{include file="../common/header.tpl"}
	
	<div style="clear:both"></div>

	<div class="safe_area">
	
	<div class="detail-main" style="height:290px">
	
		<div class="detail-main-left fl">
			<div class="card-detail-up">
				<div class="coupon-detail-pic fl">		
					<a target="_blank"  style="position:relative;display:block" title="{$card.merchant}" href="http://shop{$merchant.external_shop_id}.taobao.com/?nick_uz={$card.merchant}">
						<span class="coupon_cardlink_bltop" style="width:314px"></span>  						  			
				  		<span class="coupon_cardlink_blbot" style="width:314px"></span>
						<img src="{$card.pic_url}" />
					</a>				
				</div>
				
				<div class="detail-main-detail-info fl">
					<div class="detail-main-detail-info-content">
						<h1>{$card.name|default:""}</h1>
						<div>
							面值：<span class="denomination">{$card.denomination/100}</span> 元
							<!--  <img style="cursor:pointer" id="likebtn" class="unlike" title="喜欢" src="http://img01.taobaocdn.com/imgextra/i1/857261527/T21PLzXihXXXXXXXXX_!!857261527.gif"/>-->
							<a style="top:20px;display:none;opacity: 0; filter:alpha(opacity=0);color: #07F;position: relative;border: 1px solid #E3DEDE;padding:2px;background-color: whiteSmoke;" href="/user/favorite">现在去查看我喜欢过的优惠券>></a>
						</div>
						
						<div>
							有效期： 
							<b>{$card.start|date_format:"%Y"}</b> 年
							<b>{$card.start|date_format:"%m"}</b> 月
							<b>{$card.start|date_format:"%d"}</b> 日 至 
							<b>{$card.end|date_format:"%Y"}</b> 年
							<b>{$card.end|date_format:"%m"}</b> 月
							<b>{$card.end|date_format:"%d"}</b> 日				
						</div>
						<div>领取条件： 直接领取</div>
						
						<div>
							发券商家：<a style="color:#07F;margin-left:4px" target="_blank" title={$card.name} href="http://shop{$merchant.external_shop_id}.taobao.com/?nick_uz={$card.merchant}">{$card.merchant}</a>
							<img class="moreintro" style="height: 12px;width:12px;cursor:pointer" src="http://img02.taobaocdn.com/imgextra/i2/729839245/T265HbXoBaXXXXXXXX_!!729839245.png"/>
						</div>
						<!--
						<div class="number" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;-o-text-overflow: ellipsis;":>
							<span class="sold">{$card.sold}</span> 人兑换
							<span class="end-time"></span>
						</div>
						-->
						<div class="submit" style="margin-top:20px">
							<a class="button exchange"></a>
							<input type="hidden" value="{$nick}" class="J_name" />
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="detail-main-detail-intro-content">
			
			本券在淘宝【<a title="{$card.merchant}" style="color:#07F" href="http://shop{$merchant.external_shop_id}.taobao.com/?nick_uz={$card.merchant}" target="_blank">{$card.merchant}</a>】全场通用；<br>
			此券为{$card.denomination/100}元优惠券，全场购物满
			
			{if $card.money_condition/100!=0}
				{$card.money_condition/100}
			{else}
				{$card.denomination/100}.01
			{/if}
				
			元可直减{$card.denomination/100}元；<br>
			优惠券有效期：<b>{$card.start|date_format:"%Y"}</b> 年
					<b>{$card.start|date_format:"%m"}</b> 月
					<b>{$card.start|date_format:"%d"}</b> 日 至 
					<b>{$card.end|date_format:"%Y"}</b> 年
					<b>{$card.end|date_format:"%m"}</b> 月
					<b>{$card.end|date_format:"%d"}</b> 日，请在有效期内使用，过期无效；<br>
			每笔订单限用一张优惠券，此券不挂失，不合并，不找零，不兑换现金，不可以抵扣运费；<br>
			优惠券的最终解释权归【<a title="{$card.merchant}" style="color:#07F" href="http://shop{$merchant.external_shop_id}.taobao.com/?nick_uz={$card.merchant}" target="_blank">{$card.merchant}</a>】所有，如有任何疑问请进入商家店铺咨询客服；<br>
			聚划算及品牌特卖均不可使用此优惠券。
				
		</div>	
		
		
		<div class="detail-main-right fr">
			<div style="margin:20px 0px 35px 0px;">
				<a title="" target="_blank" href="#">
					<img src="http://img04.taobaocdn.com/imgextra/i4/729839245/T2ItrbXaNaXXXXXXXX_!!729839245.jpg" alt="" />
				</a>
			</div>
		</div>
		
	</div>
	
	<div style="clear:both"></div>
	
	<div class="moreWidget">	
		<h2>本店热卖</h2>
		<!-- 瀑布流 -->
		<div class="content_main water-contain">
			
			{foreach name=outer from=$line item=items}
			<div class="water-line-three fl">
				{foreach key=key from=$items item=detail}
			
				<div class="module" style="margin-top:20px;margin-bottom:0;">
									
					<div class="img" style="overflow:hidden;">
						<a href="http://item.taobao.com/item.htm?id={$detail.num_iid}" target="_blank" >
							<img src="{$detail.pic_url}_310x310.jpg" title="{$detail.title}" />
						</a>
					</div>
					<div style="margin-left:15px;color: #818080;display: block;height: 18px;padding-top:7px;width:260px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap">
						{$detail.title}
					</div>
					<div class="price" style="padding:0;padding:6px 0;" >
						<div class="price-left" style="height:40px;">
							<div class="original-price" style="text-decoration: none;">￥{$detail.promotion_price} 元 </div>
							<div style="margin:5px 0;margin-left: 3px;" class="original-price" style="text-decoration: none;">成交量 {$detail.volume} 件</div>
						</div>
						<div class="price-right" style="height:30px;">
							<a href="http://item.taobao.com/item.htm?id={$detail.num_iid}"
							style="margin-top: 6px;margin-right:10px;height:24px;line-height:24px;width:69px;color:#fff;background:url(http://img04.taobaocdn.com/imgextra/i4/782694284/T2wX1zXcdcXXXXXXXX_!!782694284.png);font-size:14px; text-align:center;border-radius: 5px;float: right;" target="_blank">去看看</a>
						</div>
					</div>
					<div class="yy"></div>
					<div style="clear:both"></div>
					
				</div>
				
				<div style="height:14px;margin:0 auto;width:100%;background-image:url(http://img01.taobaocdn.com/imgextra/i1/782694284/T29dKVXbtbXXXXXXXX_!!782694284.png);background-repeat:no-repeat;background-position:0 0px">
					</div>
					
				{/foreach}
			</div>
		{/foreach}
		
		<div style="clear:both"></div>
		<div style="text-align:center;margin: 10px 0;">
			<a class="moremerchbtn" href="http://shop{$merchant.external_shop_id}.taobao.com/?nick_uz={$card.merchant}" target="_blank">更多精品</a>
		</div>
		
		<!-- 瀑布流 	
		<div class="wrap" name="wrap active">
			{foreach from=$items item=item}
			<div class='mode module' style="margin-top:20px;margin-bottom:0;">
			  <p class='pic'>
			   <a href='#'><img src="{$item.item_pictrue}_310x310.jpg" style="height:auto;" /></a>
			  </p>
			  <h3 class='tit'><span><a href='http://item.taobao.com/item.htm?id={$item.track_iid}' target="_blank">{$item.item_name}</a></span></h3>
			</div>
			{/foreach}
		</div>
		-->
	</div>
	
	
</div>
	
</div>	
	
<script src="/assets/javascripts/front/coupondetail.js"></script>	
	
	
	
	
	
	
	
	
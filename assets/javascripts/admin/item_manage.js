
var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});


var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_outer").hide();
	$(".admin_inner").hide();
});

var cardDelete = DOM.query(".admin_delete");
var tr = DOM.query(".admin_flage_tr");
cardDelete.map(function(item,index){
	
	Event.on(item,'click',function(){
		
		var date = new Date();
		var itemId = DOM.query(".admin_itemId")[index].innerHTML;
		KISSY.io({
			type:"GET",
		    url:"/d/delete_item_json?t="+date,
		    data: {"id":itemId},
		    dataType:"jsonp",
			success:function(data, textStatus, xhr){
				
				if(data.result=="success"){
					
					$(".admin_flage_tr").item(index).hide(1);
					$(".admin_flage_tr").item(index).c_prev(".order-hd").fadeOut(0.5);
					$(".admin_flage_tr").item(index).c_prev(".sep-row").fadeOut(0.5);
				}
			}
		});
		
	});
	
});


var updateItemId = -1; 
var update = DOM.query(".admin_update");
update.map(function(item,index){
	
	Event.on(item,'click',function(){
		
		var title = DOM.query(".itemTitle")[index].innerHTML;
		var price = DOM.query(".itemPrice")[index].innerHTML;
		var category = DOM.query(".itemCategory")[index].innerHTML;
		var updateName = DOM.query(".admin_name")[0];
		var updateImgUrl = DOM.query(".admin_imgUrl")[0];
		var updateCategory = DOM.query(".admin_category")[0];
		var itemId = DOM.query(".admin_itemId")[index].innerHTML;
		updateName.value = title;
		updateCategory.value = category;
		updateItemId = itemId;
		
		$(".admin_outer").show();
		$(".admin_inner").show();
	});
	
});

var button = DOM.query(".admin_button")[0];
Event.on(button, 'click', function(){
	
	var updateName = DOM.query(".admin_name")[0].value;
	var updateImgUrl = DOM.query(".admin_imgUrl")[0].value;
	var updateCategory = DOM.query(".admin_category")[0].value;
	var date = new Date();
	updateName = encodeURI(updateName);
	updateCategory = encodeURI(updateCategory);
	KISSY.io({
		type:"GET",
	    url:"/d/update_item_json?t="+date,
	    data: {"iid":updateItemId,"name":updateName,"imgUrl":updateImgUrl,"category":updateCategory},
	    dataType:"jsonp",
		success:function(data, textStatus, xhr){
			
			var updateTable = DOM.query(".updateTable")[0];
			if(data.result=="success"){
				updateTable.innerHTML = "更新成功！！！";
			}else{
				updateTable.innerHTML = "更新失败！！！";
			}
		}
	});
    
});












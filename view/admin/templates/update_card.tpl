
<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		
		{include file="../common/newMessage.tpl"}
		
		<form action="/d/card/update" method="post">
			<input type="hidden" name="id" value="{$card.id}" />
			<table height="200" width="450">
				<tr>
					<td align="right">卡券名称:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="name" class="admin_search_input" value="{$card.name}" /></td>
					<td align="right">商家名称:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="merchant" class="admin_search_input" value="{$card.merchant}" disabled=false /></td>
		      	</tr>
	      		<tr>
	      			<td align="right">卡券图片:</td>
	      			<td width="10"></td>
	      			<td colspan="4"><input type="text" name="pic_url" class="admin_input" value="{$card.pic_url}" /></td>
		      	</tr>
				<tr>
	      			<td align="right">开始时间:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="start" class="admin_search_input" value={$card.start|date_format:"%Y-%m-%d"} /></td>
					<td align="right">结束时间:</td>
	      			<td width="10"></td>
	      			<td><input type="text" name="end" class="admin_search_input" value={$card.end|date_format:"%Y-%m-%d"} disabled=false /></td>
		      	</tr>
	     	 </table>
			 
			 <div class="admin_save_back" style="width:400px">
					<a><input type="submit" class="admin_save_button" value="更    新"></a>
			</div>
			 
		</form>
		
	</div>	
		
		
</div>


<script src="/assets/javascripts/admin/update_card.js"></script>
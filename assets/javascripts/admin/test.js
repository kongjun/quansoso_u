var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

var content = DOM.query('.content');
var editor = DOM.query(".edit");

content.map(function(item, index){

    Event.on(item, 'mouseenter', function(){
    	
        $(".overlay").item(index).show();
    });
    
    Event.on(item, 'mouseleave', function(){
    
        $(".overlay").item(index).hide();
    });
    
});


var editorIndex = -1;
editor.map(function(item, index){

    Event.on(item, "click", function(){
    
        $(".outer").show();
        editorIndex = index;
        
    });
    
});

var button = DOM.query(".button")[0];
Event.on(button, 'click', function(){
	
    var imgUrl = DOM.query(".imgUrl")[0].value;
    var imgWidth = DOM.query(".imgWidth")[0].value;
    //var imgHeight = DOM.query(".imgHeight")[0].value;
    var loadImg = DOM.query(".loadImg")[editorIndex];
    
    loadImg.src = imgUrl;
	$(".content").item(editorIndex).c_animate({width:imgWidth},0.7,"easeNone");
	$(".overlay").item(editorIndex).c_animate({width:imgWidth},0.7,"easeNone");
    $(".loadImg").item(editorIndex).c_animate({width:imgWidth},0.7,"easeNone");
    $(".outer").hide();
});


var close = DOM.query(".close")[0];
Event.on(close, 'click', function(){

    $(".outer").hide();
});



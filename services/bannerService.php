<?php
include '../dao/BannerDao.php';
include '../dao/BannerPositionCode.php';

/**
 * 
 * @Description 查询所有的banner位
 * @param unknown $current
 * @param unknown $size
 * @return Page
 */
function getAllBannerPage($current, $size){
	
	global $bannerDao;
	$page = $bannerDao->selectPageAll($current, $size);
	$newBanner = array();
	foreach ($page->resultList as $banner){
	
		$arr = array();
		$arr['banner'] = $banner;
		$arr['position'] = getBannerPositionByNum($banner->position)->desc;
		$newBanner[] = $arr;
	}
	$page->resultList = $newBanner;
	return $page;
}

/**
 * 
 * @Description 更新
 * @param unknown $banner
 * @return boolean
 */
function updateBannerById($banner){
	
	global $bannerDao;
	return $bannerDao->updateById($banner);
}

/**
 * 
 * @Description 通过位置获得banner
 * @param unknown $position
 * @return Ambigous <multitype:BannerDO, multitype:BannerDO >
 */
function getBannerByCode($code){
	
	global $bannerDao;
	$position = getBannerPositionByCode($code)->num;
	return $bannerDao->selectByPosition($position);
}




<?php
require_once 'BaseDO.php';

class BannerDO extends BaseDO{
	
	public $id;
	
	public $img_url;
	
	public $title;
	
	public $position;
	
	public $describe;
	
	public $link;
	
	public $memo;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}


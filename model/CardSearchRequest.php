<?php

class CardSearchRequest {
	
	public $systemParam;
	
	public $card_type;
	
	public $shop_type;
	
	public $shop_level;
	
	public $denomination_start;
	
	public $denomination_end;
	
	public $discount_rate_start;
	
	public $discount_rate_end;
	
	public $money_condition_start;
	
	public $quantity_condition_start;
	
	public $category_ids;
	
	public $end_date_start;
	
	public $end_date_end;
	
	public $reminder_start;
	
	public $reminder_end;
	
	public $sold_count_start;
	
	public $sold_count_end;
	
	public $last_modified;
	
	public $current_page;
	
	public $page_size;
	
}

?>
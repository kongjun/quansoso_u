<?php
include '../../services/cardService.php';
include '../../services/merchantService.php';
include '../../util/arrayUtil.php';
include '../../util/util.php';

if(isset($_GET['id'])){
	
	$cardDo = new CardDO();
	if(isset($_GET['id'])){
		$couponId = intval($_GET['id']);
		$cardDo = getCardById($couponId);
	}else {
		$sourceId = intval($_GET['sorceId']);
		$cardDo = getCardBySourceId($sourceId);
	}
	
	$merchant = getMerchantByName($cardDo->merchant);
	if(is_null($merchant->external_id)){
		$merchant->external_id = getMerchantIdByKeyword($merchant->name);
		$respMerchant = getMerchantShopInfo($merchant->name);
		$merchant->external_cid = $respMerchant->cid;
		$merchant->external_shop_id = $respMerchant->sid;
		$merchant->description = $respMerchant->desc;
		updateMerchantById($merchant);
	}
	$items = getMerchantRecommendItems($merchant->external_id,18);
	$line = getWaterFall($items,3);
	
}

$loginUser = $context->getBrowser();
$nick = is_null($loginUser) ? "" : $loginUser->getNick();
//$ip = getIPAddress();
$smarty = new Smarty ();

$smarty->assign("card",$cardDo);
$smarty->assign("merchant",$merchant);
$smarty->assign ("line", $line);
$smarty->assign ("nick", $nick);


$smarty->display ( "../front/templates/card_detail.tpl" );



	{include file="../common/message.tpl"}

<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		
		<div class="admin_page">
			
			<input type="text" name="title" class="admin_search_input" />&nbsp;<a><input type="submit" value="搜索" class="admin_save_button" /></a>
			
			{if $page.currentPage > 1}
			<a href="/d/activity/manage?current={$page.currentPage-1}"><input type="button" value="上一页" class="admin_save_button" /></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/activity/manage?current={$page.currentPage+1}"><input type="button" value="下一页" class="admin_save_button" /></a>
			{/if}
			<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
			
		</div>
		
		<table border="1" class="admin_table" style="width:880px;">
		    <tr class="title">
		    	<td colspan="2">活动名称</td>
				<td width="80">商家</td>
				<td width="80">开始时间</td>
				<td width="80">结束时间</td>
				<td width="80">标题</td>
				<td width="180">介绍信息</td>
				<td width="80">操作</td>
		    </tr>
			{foreach from=$page.resultList item=activity}
			<tr class="sep-row"><td colspan="9"></td></tr>
			<tr class="order-hd">
				<td colspan="9">
					活动编号：<span class="admin_itemId">{$activity.id}</span>
				</td>
			</tr>
			<tr class="order-bd admin_flage_tr">
				<td class="baobei" colspan="2">
					<a target="_blank"  href="{$activity.img_url}" class="pic">
						<img alt="{$activity.title}" src="{$activity.img_url}_310x310.jpg">
					</a>
					<div class="desc">
						<a class="baobei-name" target="_blank" href="/d/activity/detail?merchant={$activity.merchant}">
						<div class="spec">
							<span class="itemTitle">{$activity.title}</span>
						</div>
						</a>
					</div>            
				</td>
				<td class="itemPrice">{$activity.merchant} </td>
				<td class="itemCategory">{$activity.begin|date_format:"%Y-%m-%d"}</td>
				<td>{$activity.end|date_format:"%Y-%m-%d"}</td>
				<td>{$activity.headline}</td>
				<td>{$activity.explain|truncate:50:"..."}</td>
				<td>
					<p><a href="/d/activity/update?id={$activity.id}"><input type="button" value="修改" class="admin_save_button" /></a></p>
				</td>
			</tr>
			{/foreach}
		</table>
		
		<div class="admin_page">
			
			{if $page.currentPage > 1}
			<a href="/d/activity/manage?current={$page.currentPage-1}"><input type="button" value="上一页" class="admin_save_button" /></a>
			{/if}
			{if $page.currentPage < $page.totalPage}
			<a href="/d/activity/manage?current={$page.currentPage+1}"><input type="button" value="下一页" class="admin_save_button" /></a>
			{/if}
			<span class="admin_pageNumber">共 {$page.totalCount} 条记录 共 {$page.totalPage} 	页 当前在第 {$page.currentPage} 页</span>
		</div>
		
	</div>
	
</div>



<script src="/assets/javascripts/admin/activity_manage.js"></script>
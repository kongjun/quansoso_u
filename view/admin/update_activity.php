<?php
include '../../services/activityService.php';

$smarty = new Smarty ();

if(isset($_POST['title'])){
	
	$params = array();
	$params['title'] = $_POST['title'];
	$params['merchant'] = $_POST['merchant'];
	$params['img_url'] = $_POST['img_url'];
	$params['begin'] = $_POST['begin'];
	$params['end'] = $_POST['end'];
	$params['explain'] = $_POST['explain'];
	$params['content'] = $_POST['content'];
	$params['headline'] = $_POST['headline'];
	$params['id'] = $_POST['id'];
	$activityDo = new ActivityDO($params);
	$num = updateActivityById($activityDo);
	$message = $num > 0 ? "更新成功！！！" : "更新失败！！！";
	$smarty->assign("message",$message);
}


if(isset($_REQUEST['id'])){

	$id = $_REQUEST['id'];
	$activity = getActivityById($id);
	$smarty->assign("activity",$activity);
}




$smarty->assign("menu","activity_manager");

$smarty->display ("templates/update_activity.tpl");

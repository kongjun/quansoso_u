
var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});

var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_outer").hide();
	$(".admin_innner").hide();
});

var showMessage = function(){
	
	$(".admin_fadeOut_message").fadeIn(0.5);
	setTimeout(function(){
		$(".admin_fadeOut_message").fadeOut(2);
	},20);
	
}();

var first = DOM.query(".admin_select_1")[0];
first.onchange = function(){
	onLoadSelect(this);
}

function onLoadSelect(selectObj){
	
	var parentId = selectObj.value;
	if(parentId!=""){
		onLoadParam(parentId);
	}else{
		var childs = DOM.query(".admin_selectContent select");
		var isRemove = false;
		childs.map(function(item,index){
			if(isRemove){
				item.parentNode.removeChild(item);
			}
			isRemove = item==selectObj;
		});
	}
	
}

function onLoadParam(parentId){
	
	var date = new Date();
	KISSY.io({
		type:"GET",
	    url:"/d/get_category_json?t="+date,
	    data: {"parentId":parentId},
	    dataType:"jsonp",
		success:function(data, textStatus, xhr){
			
			var categories = data.categories;
			var value = "";
			categories.map(function(item,index){
				if(index==0){
					value += "<option value=''>---请选择</option>";
				}
				value += "<option value="+item.id+">"+item.name+"</option>";
			});
			var levelNum = parseInt(data.levelNum)+1;
			var className = "admin_select_"+levelNum;
			
			var selectContent = DOM.query(".admin_selectContent")[0];
			var update = DOM.query("."+className)[0];
			if(!update && value!=""){
				var select = document.createElement("select");
				select.className = className;
				select.name = "parent[]";
				select.innerHTML = value;
				select.onchange = function(){
						onLoadSelect(this);
				};
				selectContent.appendChild(select);
			}else{
				
				var childs = selectContent.childNodes;
				childs.map(function(item,index){
					
					if(index>levelNum+1){
						selectContent.removeChild(item);
					}
					
				});
				if(value==""){
					update.parentNode.removeChild(update);
				}else{
					update.innerHTML = value;
				}
				
			}
			
		}
	});
	
}





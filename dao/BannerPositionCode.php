<?php
class BannerPositionCode {
	
	public $num;
	
	public $code;
	
	public $desc;
	
	public function __construct($num,$code,$desc){
	
		$this->num = $num;
		$this->code = $code;
		$this->desc = $desc;
	}
}

$bannerPositionCodes = array();
$bannerPositionCodes[] = new BannerPositionCode(1, "INDEXROLL", "首页滚动banner");
$bannerPositionCodes[] = new BannerPositionCode(2, "TAOBAOBEI", "淘宝贝");
$bannerPositionCodes[] = new BannerPositionCode(3, "INDEXTOPBANNER", "首页横幅-享优惠下面");
$bannerPositionCodes[] = new BannerPositionCode(4, "INDEXBOTTOMBANNER", "首页横幅-底部");
$bannerPositionCodes[] = new BannerPositionCode(5, "TAOBAOBEI-1", "淘宝贝第一个菜单");
$bannerPositionCodes[] = new BannerPositionCode(6, "TAOBAOBEI-2", "淘宝贝第二个菜单");
$bannerPositionCodes[] = new BannerPositionCode(7, "TAOBAOBEI-3", "淘宝贝第三个菜单");
$bannerPositionCodes[] = new BannerPositionCode(8, "TAOBAOBEI-4", "淘宝贝第四个菜单");
$bannerPositionCodes[] = new BannerPositionCode(9, "TAOBAOBEI-5", "淘宝贝第五个菜单");
$bannerPositionCodes[] = new BannerPositionCode(10,"TAOBAOBEI-6", "淘宝贝第六个菜单");
$bannerPositionCodes[] = new BannerPositionCode(11,"GATHERACTIVITY", "聚活动头部");

function getBannerPositionByNum($num){

	global $bannerPositionCodes;
	foreach ($bannerPositionCodes as $itemCode){

		if($num==$itemCode->num){
			return $itemCode;
		}
	}
}


function getBannerPositionByCode($code){
	
	global $bannerPositionCodes;
	foreach ($bannerPositionCodes as $itemCode){
		
		if($code==$itemCode->code){
			return $itemCode;
		}
	}
	
}



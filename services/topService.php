<?php
include ("../top/topclient.php");
include ("../top/ItemsListGetRequest.php");
include ("../top/ShoprecommendItemsGetRequest.php");
include("../top/ShopGetRequest.php");
include("../top/TaobaokeItemsGetRequest.php");
include("../top/TaobaokeShopsGetRequest.php");
include("../top/TaobaokeItemsRelateGetRequest.php");

/**
 * 
 * @Description 一次获得多个商品
 * @param unknown $numiids
 * @return Ambigous <unknown, mixed>
 */
function getListItemsInfo($numiids) {
	
	global $topClient;
	$req = new ItemsListGetRequest ();
	$req->setFields ( "nick,num_iid,title,pic_url,price,cid" );
	$req->setNumIids ( $numiids );
	$resp = $topClient->execute ( $req );
	return $resp;
}

/**
 * 
 * @Description 获得店铺热卖商品（一次最多10个）
 * @param unknown $sellerId
 * @param unknown $count
 * @return Ambigous <unknown, mixed>
 */
function getRecommendShopItems($sellerId,$count){
	
	global $topClient;
	$req = new ShoprecommendItemsGetRequest();
	$req->setSellerId($sellerId);
	$req->setRecommendType(1);
	$count = $count > 10 ? 10 : $count;
	$req->setCount($count);
	$resp = $topClient->execute($req);
	return $resp;
}

/**
 * 获取店铺信息
 * @param  $nick
 * @return mixed|SimpleXMLElement
 */
function getShopInfo($nick)
{
    global $topClient;
    $req = new ShopGetRequest();
    $req->setFields("sid,cid,title,nick,desc,bulletin,pic_path");
    $req->setNick($nick);
    $resp = $topClient->execute($req);
    return  $resp;
}

/**
 * 
 * @Description 获得商品的优惠价格
 * @param unknown $keyword
 * @return Ambigous <unknown, mixed>
 */
function getItemPromotionPrice($keyword) {
	global $topClient;
	$req = new TaobaokeItemsGetRequest();
	$req->setFields("promotion_price");
	$req->setKeyword($keyword);
	return $topClient->execute($req);
}

/**
 * 
 * @Description 获得商家的user_id
 * @param unknown $keyword
 * @return Ambigous <unknown, mixed>
 */
function getTaobaokeShopByKeyword($keyword){
	
	global $topClient;
	$req = new TaobaokeShopsGetRequest();
	$req->setFields("seller_nick,shop_title,user_id,click_url,seller_credit,shop_type");
	$req->setKeyword($keyword);
	return $topClient->execute($req);
}

/**
 * 
 * @Description 获得商家热门推荐的商品
 * @param unknown $seller_id
 * @return Ambigous <unknown, mixed>
 */
function getTaobaokeRelateItem($seller_id,$maxCount){
	
	global $topClient;
	$req = new TaobaokeItemsRelateGetRequest();
	$req->setFields("num_iid,title,nick,price,promotion_price,pic_url,volume");
	$req->setRelateType(4);
	$req->setSellerId($seller_id);
	$req->setSort("commissionNum_desc");
	$req->setMaxCount($maxCount);
	return $topClient->execute($req);
}






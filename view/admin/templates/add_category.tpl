
<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		
		{include file="../common/newMessage.tpl"}
		
		<form action="/d/category/add" method="POST">
	      <table height="300">
	      		<tr>
	      			<td align="right">上一级类目:</td>
	      			<td width="10"></td>
	      			<td class="admin_selectContent">
	      				<select class="admin_select_1" name="parent[]">
	      					<option value="">---请选择</option>
	      					{foreach from=$catgories item=catgory}
							<option value={$catgory.id}>{$catgory.name}</option>
							{/foreach}
						</select>
	      			</td>
		      	</tr>
	      		<tr>
	      			<td align="right">类目名称:</td>
	      			<td width="10"></td>
	      			<td><textarea  cols="20" rows="10" name="names"></textarea>(注：多个换行)</td>
		      	</tr>
	      		<tr>
	      			<td></td>
	      			<td width="10"></td>
	      			<td><a href="#"><input class="admin_save_button" type="submit" value="提交" /></a></td>
		      	</tr>
	      </table>
	   </form>
		
	</div>
	
</div>

<script src="/assets/javascripts/admin/category_manage.js"></script>


<div class="quansoso_intaobao_warp"><!-- 头部 --> 
	{include file="../common/header.tpl"}
	
	<div style="clear: both"></div>
	
	<div class="safe_area">
	
	<div class="kup-menu-second">
		<div class="head_shader_second"></div>
		{foreach from=$categories item=category name=foo}
			<a href="/d/goods?category={$category.id}&menu={$smarty.foreach.foo.index+1}" {if $categoryId eq $category.id}class="fontRed"{/if}>{$category.name}</a>
			{if  !$smarty.foreach.foo.last}
				<span>|</span>	
			{/if}
			
		 {/foreach}
	</div>
	<div class="goods-banner">
		<a href="{$banner.link}" title="{$banner.title}"><img class="goods-banner-img" src="{$banner.img_url}" alt="{$banner.title}"></a>
		<!--
		<div class="goods-banner-num">
		{foreach from=$banners item=banner}	
			<a class="goods-banner-numbtn" href="#">{$banner.describe}</a>
		{/foreach}	
		</div>
		-->
	</div>

	<div class="moreWidget">	

		<!-- 瀑布流 -->
		<div class="content_main water-contain">
			
			<div class="life-tag">
				<ul>
				{foreach from=$tagCategory item=tagCategory}
					{if $tagCategory.name}
					<li>
						<a href="/d/goods?category={$tagCategory.id}&menu={$menu}" class="href-Tag" {if $categoryId==$tagCategory.id}style="color:white;background-color: #656F86;"{/if} >{$tagCategory.name}</a>
					</li>
					{/if}
				{/foreach}
				</ul>
			</div>
			
			
			{foreach name=outer from=$line item=items}				
				
					{if $smarty.foreach.outer.last}
						<div class="water-line fl" style="margin-right:0px;">						
					{else if}	
						<div class="water-line fl" style="margin-right:18px;">
					{/if}
				
					{foreach key=key from=$items item=detail}
				
					<div class="good-show">
						<input type="hidden" value="{$detail.item.iid}" class="J_iid" />	
						<div class="img-box" style="overflow:hidden;">
							<a class="good-link" href="http://item.taobao.com/item.htm?id={$detail.item.iid}" target="_blank" >
								<img class="good-link-img" src="{$detail.item.image_url}_310x310.jpg" title="{$detail.item.title}" />
							</a>
						</div>
						
						<div style = "background:#fff;height:75px">
						<div style="margin-left:15px;display: block;height: 18px;padding-top:7px;width:200px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap">
							<a href="http://item.taobao.com/item.htm?id={$detail.item.iid}" style="color:#818080;" target="_blank" >{$detail.item.title}</a>
						</div>
					
						<div class="good-info">
							<span class="good-price">
								<span class="J_oprice">￥{$detail.item.price}</span>
							</span>
						
							
							<div style="clear:both"></div>
							{if $detail.coupon}
								<div class="gmoreinter">
									{foreach from=$detail.coupon item=coupon name=coupons}
										{if $smarty.foreach.coupons.index ==0}
										<span><a target="_blank" href="/d/coupon?id={$coupon.id}">用券立省{$coupon.denomination/100}元</a></span>
										{/if}
									{/foreach}
										<!-- if this good has more coupons -->
									<div class="gmorecoupons">
										<div class="toparrow_icon"></div>
										<ul>
											{foreach from=$detail.coupon item=coupon name=coupons}
												<li>											
													<a target="_blank" href="/d/coupon?id={$coupon.id}">
													{if $coupon.money_condition==0}
														{$coupon.denomination/100}元现金券
													{else}
														满{$coupon.money_condition/100}减{$coupon.denomination/100}
													{/if}
													</a>
												</li>
											{/foreach}
										</ul>
									</div>
								
								</div>
							{/if}
							
							<div class="good-volume">
								<a href="http://item.taobao.com/item.htm?id={$detail.item.iid}"
								style="height:24px;line-height:24px;width:69px;color:#fff;background:url(http://img04.taobaocdn.com/imgextra/i4/782694284/T2wX1zXcdcXXXXXXXX_!!782694284.png);text-align: center;border-radius: 5px;float: right;font-size:12px" target="_blank">去看看</a>
							</div>
							
							
						</div>	
						
						</div>
						<img class="shader-img" src="http://img01.taobaocdn.com/imgextra/i1/782694284/T29dKVXbtbXXXXXXXX_!!782694284.png"/>
						
									
					</div>
					
					
						
					{/foreach}
				</div>
			{/foreach}
			
		</div>
		
		<div class="baobei-pagination fm950">
	    	<div class="pagination-wrap">
	    		{if $page.currentPage > 1}
	    			<a  href="/d/goods?category={$categoryId}&current={$page.currentPage-1}&menu={$menu}" class="baobei-next flxx">上一页</a>
	    		{/if}
				{foreach from=$pageArray item=arr}
					
					{if $page.currentPage==$arr}
						<a class="c">{$arr}</a>
					{else}
						<a href="/d/goods?category={$categoryId}&current={$arr}&menu={$menu}" class="flxx">{$arr}</a>
					{/if}
				
					
				{/foreach}
				
				{if $page.currentPage < $page.totalPage}
					<a href="/d/goods?category={$categoryId}&current={$page.currentPage+1}&menu={$menu}" class="baobei-next flxx">下一页</a>
				{/if}
	            <div style="clear:both"></div>
	        </div>
	    </div>

	</div>
</div>
</div>	

<script src="/assets/javascripts/front/goodslist.js"></script>		
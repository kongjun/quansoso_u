<?php
include '../model/GoodItemDO.php';
include '../model/Page.php';

class GoodItemDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description 插入宝贝
	 * @param unknown $goodItem
	 * @return number
	 * @return number
	 */
	public function insertGoodItems($goodItem){
		
		$sql = "insert into ekp_good_item (merchant_name,top_cid,ekp_cid,iid,title,image_url,price,code,gmt_created) values (?,?,?,?,?,?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($goodItem->merchant_name,$goodItem->top_cid,$goodItem->ekp_cid,$goodItem->iid,$goodItem->title,$goodItem->image_url,$goodItem->price,$goodItem->code,$goodItem->gmt_created));
	}
	
	/**
	 * 
	 * @Description 查询所有商品
	 * @param unknown $current
	 * @param unknown $order
	 * @return Page
	 */
	public function selectAllItems($current,$order)
	{
		$items = array();
		$sql = "select count(*) from ekp_good_item";
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page(10,$current,$totalCount);
	
		$sql = "select * from ekp_good_item order by $order desc limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($page->currentPage-1)*$page->pageSize,$page->pageSize));
		while (@$row=$exec->fetch()){
			$items[] = new GoodItemDO($row);
		}
		$page->resultList = $items;
		return $page;
	}
	
	/**
	 * 
	 * @Description 删除商品
	 * @param unknown $id
	 */
	public function deleteItemByIid($id){
		
		$sql = "delete from ekp_good_item where iid = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($id));
	}
	
	/**
	 * 
	 * @Description 通过名称查询商品---Page分页
	 * @param unknown $name
	 * @return GoodItemDO
	 */
	public function selectPageItemLikeTitle($name){
		
		$sql = "select count(*) from ekp_good_item where title like ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array("%".$name."%"));
		$totalCount = $exec->fetchColumn();
		$page = new Page(10,1,$totalCount);
		$sql = "select * from ekp_good_item where title like ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array("%".$name."%"));
		$items = array();
		while (@$row=$exec->fetch()){
			$items[] = new GoodItemDO($row);
		}
		$page->resultList = $items;
		return $page;
	}
	
	/**
	 * 
	 * @Description 通过类目查询商品---Page分页
	 * @param unknown $cids
	 * @param unknown $pageSize
	 * @param unknown $current
	 * @return Page
	 */
	public function selectPageItemInEkpCid($cids,$pageSize,$current){
		
		$sql = "select count(*) from ekp_good_item where ekp_cid in($cids)";
		$totalCount = $this->pdo->query($sql)->fetchColumn();
		$page = new Page($pageSize, $current, $totalCount);
		
		$sql = "select * from ekp_good_item where ekp_cid in ($cids) order by gmt_created desc,iid%5 limit ?,?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array(($current-1)*$pageSize,$pageSize));
		$items = array();
		while (@$row=$exec->fetch()){
			$item = new GoodItemDO($row);
			$item->price = sprintf("%0.2f",$item->price/100);
			$items[] = $item;
		}
		shuffle($items);
		$page->resultList = $items;
		return $page;
	}
	
	/**
	 * 
	 * @Description 更新
	 * @param unknown $item
	 */
	public function updateByItem($item){
		
		$sql = "update ekp_good_item set title=?,ekp_cid=?,image_url=? where `iid` = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($item->title,$item->ekp_cid,$item->image_url,$item->iid));
	}
	
	/**
	 * 
	 * @Description 通过id查询商品
	 * @param unknown $id
	 * @return GoodItemDO
	 */
	public function selectByIid($iid){
		
		$sql = "select * from ekp_good_item where iid = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($iid));
		return new GoodItemDO($exec->fetch());
	}
	
}

$goodItemDao = new GoodItemDao();

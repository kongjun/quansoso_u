
<div class="admin_wrap">
	
	<div class="admin_left">
	
		{include file="../common/menu.tpl"}
	
	</div>
	
	<div class="admin_right">
		
		{if $message}
			<div class="admin_fadeOut_message" style="top:14px;">{$message}</div>
		{/if}
		
		<form action="/d/item/add" method="POST">
	      	<table height="400">
	      		<tr>
	      			<td align="right">类目:</td>
	      			<td width="10"></td>
	      			<td class="admin_selectContent">
	      				<select class="admin_select_1" name="parent[]">
	      					{foreach from=$catgories item=catgory}
							<option value={$catgory.id}>{$catgory.name}</option>
							{/foreach}
						</select>
	      			</td>
		      	</tr>
	      		<tr>
	      			<td align="right">请输入商品:</td>
	      			<td width="10"></td>
	      			<td>
	      				<textarea name="items" cols="35" rows="18"></textarea>
						说明：每个商品之间换行
					</td>
		      	</tr>
				<tr>
	      			<td></td>
	      			<td width="10"></td>
	      			<td><a><input type="submit" value="提交" class="admin_save_button" /></a></td>
		      	</tr>
	     	 </table>
	   </form>
		
		
	</div>
	
</div>

<script src="/assets/javascripts/admin/add_good_recommend.js"></script>
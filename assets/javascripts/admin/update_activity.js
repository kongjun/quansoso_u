
var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});

var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_outer").hide();
	$(".admin_innner").hide();
});

var prevImg = DOM.query(".admin_preview_img")[0];
Event.on(prevImg,'click',function(){
	
	var img = DOM.query(".admin_img_prev")[0].value;
	var title = DOM.query(".admin_prev_title")[0].value;
	var explain = DOM.query(".admin_explain_prev")[0].value;
	var headline = DOM.query(".admin_prev_headline")[0].value;
	var imgPrev = DOM.query(".admin_prevImg")[0];
	var headLinePrev = DOM.query(".admin_prevHeadLine")[0];
	var explainPrev = DOM.query(".admin_prevExplain")[0];
	imgPrev.src = img;
	imgPrev.alt = title;
	imgPrev.title = title;
	explainPrev.innerHTML = explain;
	headLinePrev.innerHTML = headline;
	$(".admin_preview_outer").show();
	$(".admin_preview_inner").show();
});

var previewClose = DOM.query(".admin_preview_close")[0];
Event.on(previewClose, 'click', function(){

    $(".admin_preview_outer").hide();
	$(".admin_preview_inner").hide();
});


var content = DOM.query(".admin_preview_content")[0];
Event.on(content,'click',function(){
	
	var form = DOM.query(".admin_form")[0];
	var prevHtml = DOM.query(".admin_content_html")[0].value;
	var prevContent = DOM.query(".admin_content_prev")[0];
	prevContent.value = prevHtml;
	form.submit();
});


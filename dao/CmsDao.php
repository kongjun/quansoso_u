<?php
include '../model/CmsDO.php';

class CmsDao {
	
	protected $pdo;
	
	public function __construct()
	{
		global $PDO;
		$this->pdo = $PDO;
	}
	
	/**
	 * 
	 * @Description 添加CMS
	 * @param unknown $cmsDo
	 * @return boolean
	 */
	public function insert($cmsDo){
		
		$sql = "insert into ekp_cms (content,CODE,memo,gmt_created) values(?,?,?,?)";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($cmsDo->content,$cmsDo->code,$cmsDo->memo,$cmsDo->gmt_created));
	}
	
	/**
	 * 
	 * @Description Enter 通过code获得关于这个code的第一个cms
	 * @param unknown $code
	 * @return CmsDO
	 */
	public function selectOrderByCode($code){
		
		$sql = "select * from ekp_cms where code = ? order by gmt_modified desc";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($code));
		return new CmsDO($exec->fetch());
	}
	
	/**
	 * 
	 * @Description 查询通过code
	 * @param unknown $code
	 */
	public function selectAllByCode($code){
		
		$sql = "select * from ekp_cms where code = ?";
		$exec = $this->pdo->prepare($sql);
		$exec->execute(array($code));
		$cmses = array();
		while (@$row=$exec->fetch()){
			$cms = new CmsDO($row);
			//$cms->content = preg_replace("/href/","data-url",html_entity_decode($cms->content));
			$cms->content = html_entity_decode($cms->content);
			$cmses[] = $cms;
		}
		return $cmses;
	}
	
	public function updateContentById($id,$content,$subTemplate){
		
		$sql = "update ekp_cms set content=?,sub_template=? where id = ?";
		$exec = $this->pdo->prepare($sql);
		return $exec->execute(array($content,$subTemplate,$id));
	}
}

$cmsDao = new CmsDao();

<?php
include '../../services/goodItemService.php';
include '../../services/categoryService.php';

$current = isset($_GET['current']) ? intval($_GET['current']) : 1;
$itemName = $_POST['title'];
if(isset($itemName) && !empty($itemName)){
	$page = getPageItemLikeTitle($itemName);
}else{
	$page = getAllItem($current,"gmt_created");
}

$items = array();
foreach ($page->resultList as $item){
	
	$itemArr = array();
	$itemArr['item'] = $item;
	$itemArr['category'] = getCategoryById($item->ekp_cid);
	$items[] = $itemArr;
}

$smarty = new Smarty ();

$smarty->assign("page",$page);
$smarty->assign("items",$items);
$smarty->assign("menu","item_manager");

$smarty->display ("templates/item_manage.tpl");


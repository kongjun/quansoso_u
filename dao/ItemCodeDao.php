<?php

class ItemCode {
	
	public $num;
	
	public $code;
	
	public $name;
	
	public function __construct($num,$code,$name){
		
		$this->num = $num;
		$this->code = $code;
		$this->name = $name;
	}
	
}

$itemCodes = array();
$itemCodes[] = new ItemCode(1,"GOODRECOMMEND","良品购");
$itemCodes[] = new ItemCode(2,"INDEXCOUPON","首页优惠券");
$itemCodes[] = new ItemCode(3,"INDEXACTIVITY","首页推荐活动");

function getItemByCode($code){
	
	global $itemCodes;
	foreach ($itemCodes as $itemCode){
		if($code==$itemCode->code){
			return $itemCode->num;
		}
	}
		
}











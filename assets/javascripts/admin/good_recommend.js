
var DOM = KISSY.DOM;
var Event = KISSY.Event;
var $ = KISSY.all;

/**
 * 默认打开某个子菜单
 */
var menuCode = DOM.query(".menu_code")[0];
var showMenu = function(){

    var code = menuCode.value;
    if (code == "cms") {
        $(".dropdown").item(0).show(0.5);
    }else if (code=="item_manager"){
		$(".dropdown").item(1).show(0.5);
	}else if (code=="card"){
		$(".dropdown").item(2).show(0.5);
	}else if(code=="activity_manager"){
		$(".dropdown").item(3).show(0.5);
	}else {
		$(".dropdown").item(4).show(0.5);
	}
    
}();

/**
 * 菜单显示子菜单的效果
 */
var menuButtons = DOM.query(".button");
menuButtons.map(function(item, index){

    Event.on(item, 'click', function(){
    
        var bool = DOM.hasClass(item, "isSelector");
        $(".dropdown").hide(1);
        if (bool) {
            $(".dropdown").item(index).hide(1);
        }
        else {
            $(".dropdown").item(index).show(1);
        }
        $(".button").removeClass("isSelector");
        DOM.addClass(item, "isSelector");
    });
});

/**
 * tab效果
 */
var tabs = DOM.query(".admin_tab_title");
var editorTab = 0;

tabs.map(function(item, index){

    Event.on(item, 'click', function(e){
    
        $(".admin_tab_title").removeClass("admin_tab_active");
        $(".admin_tab_title").item(index).c_addClass("admin_tab_active");
        $(".admin_tab_subContent").hide();
        $(".admin_tab_subContent").item(index).show();
        editorTab = index;
    });
    
});


var editorIndex = -1;
var boards = DOM.query(".board");
boards.map(function(item, index){

    Event.on(item, 'click', function(){
    	
		var param = DOM.query(".admin_param")[0];
		param.value = "";
        $(".admin_outer").show();
        editorIndex = index;
		return false;
    });
    
});

var button = DOM.query(".admin_button")[0];
Event.on(button, 'click', function(){
	
	var param = DOM.query(".admin_param")[0].value;
	var paramArr = param.split(" ");
	var iid = paramArr[0];
	var date = new Date();
	var mess = DOM.query(".message")[0];
	mess.innerHTML = "";
	KISSY.io({
		type:"GET",
	    url:"/d/get_recommend_cms_json?t="+date,
	    data: {"iid":iid},
	    dataType:"jsonp",
		success:function(data, textStatus, xhr){
			
			if(data.result=="success"){
				
				var param = DOM.query(".admin_param")[0].value;
				var paramArr = param.split(" ");
				var iid = paramArr[0];
				var imgUrl = paramArr[1];
				var tag = paramArr[2];
				var title = decodeURI(data.title);
				var board = DOM.query(".board")[editorIndex];
				var aHref = board.childNodes[1];
				aHref.href = "http://item.taobao.com/item.htm?id="+iid;
			    aHref.childNodes[1].innerHTML = tag;
			    aHref.childNodes[3].src = imgUrl+"_310x310.jpg";
				aHref.childNodes[3].alt = title;
				aHref.childNodes[3].title = title;
				$(".admin_outer").hide();
				
			}else{
				mess.innerHTML = "获取数据失败，请重试。。。";
			}
		}
	});
});	

var close = DOM.query(".admin_close")[0];
Event.on(close, 'click', function(){

    $(".admin_outer").hide();
});


var save = DOM.query(".admin_save")[0];
Event.on(save, 'click', function(){

    var content = DOM.query(".admin_content")[editorTab];
	var id = DOM.query(".admin_cms_id")[editorTab];
    var contentHtml = DOM.query(".content_html")[0];
    contentHtml.value = content.innerHTML;
	DOM.query(".J_cms_id")[0].value = id.value;
	DOM.query(".J_cms_templateId")[0].value = editorTab;
});

var preview = DOM.query(".admin_preview")[0];
Event.on(preview,'click',function(){
	
	var content = DOM.query(".admin_content")[editorTab];
	var previewHtml = DOM.query(".admin_preview_html")[0];
	previewHtml.innerHTML = content.innerHTML;
	$(".admin_preview_outer").show();
	$(".admin_preview_inner").show();
});

var previewClose = DOM.query(".admin_preview_close")[0];
Event.on(previewClose, 'click', function(){

    $(".admin_preview_outer").hide();
	$(".admin_preview_inner").hide();
});

var showMessage = function(){
	
	$(".admin_fadeOut_message").fadeIn(0.5);
	setTimeout(function(){
		$(".admin_fadeOut_message").fadeOut(2);
	},20);
	
}();




<?php

class SystemParam {
	
	public $service;
	
	public $appkey;
	
	public $input_charset;
	
	public $sign_type;
	
	public $sign;
	
	public $secret;
	
	public function __construct($service){
		
		$this->appkey = "38760583";
		$this->secret = "di3dbut1kjutp2x80fre9un8croklocv";
		$this->input_charset = "UTF-8";
		$this->sign_type = "NEWMD5";
		$this->service = $service;
	}
	
	/**
	 * @return the $service
	 */
	public function getService() {
		return $this->service;
	}

	/**
	 * @return the $appkey
	 */
	public function getAppkey() {
		return $this->appkey;
	}

	/**
	 * @return the $input_charset
	 */
	public function getInput_charset() {
		return $this->input_charset;
	}

	/**
	 * @return the $sign_type
	 */
	public function getSign_type() {
		return $this->sign_type;
	}

	/**
	 * @return the $sign
	 */
	public function getSign() {
		return $this->sign;
	}

	/**
	 * @param field_type $service
	 */
	public function setService($service) {
		$this->service = $service;
	}

	/**
	 * @param field_type $appkey
	 */
	public function setAppkey($appkey) {
		$this->appkey = $appkey;
	}

	/**
	 * @param field_type $input_charset
	 */
	public function setInput_charset($input_charset) {
		$this->input_charset = $input_charset;
	}

	/**
	 * @param field_type $sign_type
	 */
	public function setSign_type($sign_type) {
		$this->sign_type = $sign_type;
	}

	/**
	 * @param field_type $sign
	 */
	public function setSign($sign) {
		$this->sign = $sign;
	}

}


<?php
require_once 'BaseDO.php';

class MerchantDO extends BaseDO {
	
	public $id;
	
	public $name;
	
	public $pic_url;
	
	public $external_id;
	
	public $external_shop_id;
	
	public $type;
	
	public $external_cid;
	
	public $external_category;
	
	public $external_level;
	
	public $brand;
	
	public $website_url;
	
	public $keywords;
	
	public $description;
	
	public $status;
	
	public $memo;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}


<?php
require_once ('BaseDO.php');

class CategoryDO extends BaseDO{
	
	public $id;
	
	public $name;
	
	public $level_code;
	
	public $parent_id;
	
	public $level_num;
	
	public $is_menu;
	
	public $gmt_created;
	
	public $gmt_modified;
	
}


<?php

/**
 * 
 * @Description 远程请求类
 * @ClassName HttpClient
 * @author kongjun
 * @date 2013-3-13 下午02:34:44
 *
 */
class HttpClient {
	
	private $ch;
	
	private $url;
	
	public function __construct($url) {
		$this->ch = curl_init ();
		$this->url = $url;
	}
	
	/**
	 * 
	 * @Description post请求
	 * @param @param String $param
	 * @param @param int $timeOut
	 * @return String
	 */
	public function remoteByPost($param, $timeOut = 10) {
		
		curl_setopt ( $this->ch, CURLOPT_URL, $this->url );
		curl_setopt ( $this->ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $this->ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $this->ch, CURLOPT_CONNECTTIMEOUT, $timeOut );
		curl_setopt ( $this->ch, CURLOPT_POST, 1 ); //设置post请求
		curl_setopt ( $this->ch, CURLOPT_POSTFIELDS, $param ); //param为请求的参数
		$file_contents = curl_exec ( $this->ch );
		curl_close ( $this->ch );
		return $file_contents;
	}
	
	/**
	 * 
	 * @Description 批量请求获得内容
	 * @param @param array $urls
	 * @param @param String $param
	 * @return String
	 */
	public function remoteByRolling($urls, $param) {
		
		if (! is_array ( $urls ) || count ( $urls ) == 0)
			return false;
		$queue = curl_multi_init ();
		$map = array ();
		foreach ( $urls as $url ) {
			$this->remoteByPost ( $url, $param );
			curl_multi_add_handle ( $queue, $this->ch );
			$map [$url] = $this->$ch;
		}
		
		$active = null;
		// 执行批处理句柄
		do {
			$mrc = curl_multi_exec ( $queue, $active );
		} while ( $mrc == CURLM_CALL_MULTI_PERFORM );
		
		while ( $active > 0 && $mrc == CURLM_OK ) {
			//curl_multi_select:等待所有cURL批处理中的活动连接
			if (curl_multi_select ( $queue, 0.5 ) != - 1) {
				do {
					$mrc = curl_multi_exec ( $queue, $active );
				} while ( $mrc == CURLM_CALL_MULTI_PERFORM );
			}
		}
		
		/* get data */
		$responses = array ();
		foreach ( $map as $url => $ch ) {
			
			if (curl_error ( $ch ) == '') {
				$responses [$url] = curl_multi_getcontent ( $ch ) ;
			}else{
				error_log("curl error");
			}
			curl_multi_remove_handle ( $queue, $ch );
			curl_close ( $ch );
		}
	}

}


